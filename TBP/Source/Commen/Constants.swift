
import Foundation
import UIKit

     var appDelegate = UIApplication.shared.delegate as! AppDelegate
     var navBarHeight = 64.0
     var backBtnBool = false

     let kScreenWidth = UIScreen.main.bounds.width
     let kScreenHeight = UIScreen.main.bounds.height

    //Local data constants
     let kUserLoginInfo = "user_login_info"

    // TextField Validation
     let kALERT                        =   "Alert!"
     let kError_Title                  =   "Error!"
     let kSuccess                      =   "Success"

     let kUser_Name                    =   "User name is required."
     let kEmpty_Email                  =   "Email Id is required."
     let kInvalid_Email                =   "Please enter valid email."

     let kEmpty_Old_Password           =   "Old Password is required."
     let kEmpty_Password               =   "Password is required."
     let kEmpty_C_Password             =   "Confirm Password is required."
     let kInvalid_Password             =   "Password must be alphanumeric and minimum length should be 6 digits."
     let kConfirm_Password             =   "Password didn't match."
     let kTermsConditions              =   "Please accept terms and conditions."

    // WebService Responce
     let kNetworkError                 =   "Network Error!"
     let kNetworkMessage               =   "The Internet connection appears to be offline."
    
     let kLogOut_Message               =   "Are you sure want to log out?"    

    //OTHER
    let kCompany                      =   "company"
    let kInfluencer                   =   "influencer"

    //Colors
    let kCompanyColor                 =    UIColor.hexColor(hex: "F1592A", alpha: 1.0)
    let kInfluencerColor              =    UIColor.hexColor(hex: "ee2b7a", alpha: 1.0)
    let kSelectedBtnColor             =    UIColor.hexColor(hex: "EEEEEE", alpha: 1.0)
    let kDefaultBtnColor              =    UIColor.white
    var kNavColor                     =    UIColor.clear

    //APIS
    let BASE_URL                      =   "http://impetrosys.com/energiia.co.in/tbp/webservices/service.php"//"http://energiia.co.in/tbp/webservices/service.php"
    let kFirst_Name                   =   "First name is required."
    let kLast_Name                    =   "Last name is required."
    let kCompany_Name                 =   "Company name is required."
    let kProduct_Type                 =   "Product type is required."
    let kAbout                        =   "Please write something about you."
    let kBranch_Name                  =   "Branch name is required."
    let kCountry_URL                  =   "Country name is required."
    let kCity_URL                     =   "City name is required."
    let kGender                       =   "Please select gender."

    let kAdMobKey                     =    "ca-app-pub-2829694823315956~3376988560"
    let kAdInterstitialKey            =    "ca-app-pub-4514287475704546/5818698510"

struct INSTAGRAM_IDS {
    
    static let INSTAGRAM_AUTHURL = "https://api.instagram.com/oauth/authorize/"
    static let INSTAGRAM_APIURl  = "https://api.instagram.com/v1/users/"
    
//    static let INSTAGRAM_CLIENT_ID  = "6cf64eb478274589ac636981b9207b60"
//    static let INSTAGRAM_CLIENTSERCRET = "1abd5cd1d69e461885d558ac836a8694"
    
    static let INSTAGRAM_CLIENT_ID  = "14cd98c190644ddea1be3a037d5aa7a4"
    static let INSTAGRAM_CLIENTSERCRET = "bdb18a73f98e41aaa41d3b5c35825d74"
    
    static let INSTAGRAM_REDIRECT_URI = "http://www.impetrosys.com/"
    static let INSTAGRAM_ACCESS_TOKEN =  "access_token"
    static let INSTAGRAM_SCOPE = "likes+comments+relationships+public_content"
    
}

