//
//  SharedData.swift
//  ViewAnimation
//
//  Created by Ranjana on 06/10/16.
//  Copyright © 2016 Ranjana. All rights reserved.
//

import UIKit
import AVFoundation
import NVActivityIndicatorView

var user_login_info: NSDictionary = NSDictionary()
var userId = ""
var userFullName = ""
var userImage = ""
var notificationData: NSDictionary = NSDictionary()
var badgeCount = 0


class SharedData: NSObject {
    
    static var instance : SharedData = {
        let instance = SharedData()
        return instance
    }()
    
    var selected_user_type = kCompany
    var fireInstanceId = ""
    
    func saveLoginUserInfoInLacal(_ loginUserInfo: [String: Any]) {
        UserDefaults.standard.set(loginUserInfo, forKey: kUserLoginInfo)
        
        if UserDefaults.standard.value(forKey: kUserLoginInfo) != nil{
            user_login_info = UserDefaults.standard.value(forKey: kUserLoginInfo) as! NSDictionary
            userId = user_login_info.value(forKey: "user_id") as! String
            userFullName = user_login_info.value(forKey: "fullName") as! String
            userImage = user_login_info.value(forKey: "image") as! String
        }
    }
    
    static func openURLonWeb(_ url: URL) {
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
    }
    
    static func PerformLogOutUser() {
        let param = "{\"data\":{\"userId\":\"\(userId)\"},\"function_name\":\"logoutUser\"}"
        
        APIManager.apiRequest(Parameters: param) { jsonResponse, error in
            if jsonResponse?.count != 0 {
                
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
                if jsonResponse?.value(forKey: "msg_code") as? Bool == true {
                    backBtnBool = false
                    UserDefaults.standard.removeObject(forKey: kUserLoginInfo)
                    UserDefaults.standard.removeObject(forKey: "sent_request_counter")
                    user_login_info = [:]
                    userId = ""
                    userFullName = ""
                    userImage = ""
                    SharedData.instance.selected_user_type  = ""
                    
                    let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                    let reveal = (mainStoryBoard.instantiateViewController(withIdentifier: "login"))
                    appDelegate.window?.rootViewController = reveal
                } else {
                    if jsonResponse!.value(forKey: "message") != nil{
                        let msg : String = (jsonResponse!.value(forKey: "message") as? String)!
                        if(appDelegate.window?.rootViewController?.currentReachabilityStatus == .notReachable) {
                            appDelegate.window?.rootViewController?.showAlert(title: kError_Title, message: msg)
                        } else {
                            appDelegate.window?.rootViewController?.showAlertInternet(title: kError_Title, message: msg, handler: {(alert: UIAlertAction!) in
                                self.PerformLogOutUser()
                            })
                        }
                    }
                }
            }
            return
        }
        
    }

  }
