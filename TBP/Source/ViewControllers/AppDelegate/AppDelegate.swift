//
//  AppDelegate.swift
//  TBP
//
//  Created by mac on 06/10/17.
//  Copyright © 2017 Impetrosys. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import IQKeyboardManagerSwift
import Firebase
import UserNotifications
import GoogleMobileAds

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate{

    var window: UIWindow?
    var willPresent = false
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        if UIScreen.main.nativeBounds.height == 2436.0{
            navBarHeight = 88.0
        }
        
        UINavigationBar.appearance().titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
        IQKeyboardManager.sharedManager().enable = true
        
        // Initialize Facebook sign-in
        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        
       var initial = "login"
        
        if UserDefaults.standard.value(forKey: kUserLoginInfo) != nil{
            user_login_info = UserDefaults.standard.value(forKey: kUserLoginInfo) as! NSDictionary
//            print(user_login_info)
            userId = user_login_info.value(forKey: "user_id") as! String
            userFullName = user_login_info.value(forKey: "fullName") as! String
            userImage = user_login_info.value(forKey: "image") as! String
            
            initial = user_login_info.value(forKey: "type") as! String
            SharedData.instance.selected_user_type  = initial
        }
        let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
        let reveal = (mainStoryBoard.instantiateViewController(withIdentifier: initial))
        self.window?.rootViewController = reveal 
        
        // Initialize FCM
        FirebaseApp.configure()
        
        if let refreshedToken = InstanceID.instanceID().token() {
            SharedData.instance.fireInstanceId = refreshedToken
           print("firebase token: \(refreshedToken)")
        }
        
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        application.registerForRemoteNotifications()
        
        // Initialize the Google Mobile Ads SDK.
        GADMobileAds.configure(withApplicationID: kAdMobKey)
        
        return true
    }
    
    //MARK: - Connect device to FCM.
   
    private func application(application: UIApplication, didReceiveRemoteNotification userInfo: [NSObject : AnyObject], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        Messaging.messaging().appDidReceiveMessage(userInfo)
    }
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
       
        if willPresent == false {
            let userInfo = response.notification.request.content.userInfo
            
            if userInfo["from_id"] != nil {
                let uid: String = userInfo["from_id"] as! String;
                let unm: String = userInfo["fromname"] as! String;
                let img: String = userInfo["from_image"] as! String;
                notificationData = ["user_id": uid, "user_name": unm, "image": img]
                self.sendToAppropriateScreen()
            }
        }
    }
    
    func sendToAppropriateScreen() {
        // rootViewController from StoryBoard
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let navigationController = mainStoryboard.instantiateViewController(withIdentifier: SharedData.instance.selected_user_type)
        self.window!.rootViewController = navigationController
    }
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void)
    {
        let userInfo = notification.request.content.userInfo
        willPresent = true
        
        if userInfo["aps"] != nil {
            let aps: NSDictionary = userInfo["aps"] as! NSDictionary
            let badge = aps.value(forKey: "badge") as! Int
//            if badge > 1 {
                badgeCount = badgeCount + badge
//            } else {
//                badgeCount = badge
//            }
            NotificationCenter.default.post(name: Notification.Name("updateBadgeCount"), object: nil)
        }
        
        if userInfo["from_id"] != nil {
            let uid: String = userInfo["from_id"] as! String;
            let unm: String = userInfo["fromname"] as! String;
            let img: String = userInfo["from_image"] as! String;
            notificationData = ["user_id": uid, "user_name": unm, "image": img]
            
            // Post notification
            NotificationCenter.default.post(name: Notification.Name("GotoMessageVC"), object: nil)
        }
      //Handle the notification
        completionHandler(
            [.alert, .sound, .badge])
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
    
        //Or let firebase find out the type of build:
        InstanceID.instanceID().setAPNSToken(deviceToken, type: .unknown)
        
        let tokenString = String(format: "%@", deviceToken as CVarArg)
            .trimmingCharacters(in: CharacterSet(charactersIn: "<>"))
            .replacingOccurrences(of: " ", with: "")
        
        print("tokenString: \(tokenString)")
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Registration failed!: ", error)
    }
    
    //MARK: - Application Life Cycle
    
    func applicationWillResignActive(_ application: UIApplication) {
        FBSDKAppEvents.activateApp()
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        return FBSDKApplicationDelegate.sharedInstance().application(application, open: url, sourceApplication: sourceApplication, annotation: annotation)
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        return FBSDKApplicationDelegate.sharedInstance().application(app, open: url, options: options)
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        Messaging.messaging().disconnect()
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        
        reconnectToFcm()
        application.applicationIconBadgeNumber = 0
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
//ReConnect to FCM
    func reconnectToFcm() {
        Messaging.messaging().connect { (error) in
            if (error != nil) {
                print("Unable to connect with FCM. \(String(describing: error))")
            } else {
                print("Connected to FCM.")
            }
        }
    }
    
}

