//
//  InboxVC.swift
//  WMA
//
//  Created by mac on 23/10/17.
//  Copyright © 2017 Impetrosys. All rights reserved.
//

import UIKit
import GoogleMobileAds
import NVActivityIndicatorView

class InboxVC: UIViewController, UITableViewDelegate, UITableViewDataSource, GADInterstitialDelegate {

    @IBOutlet weak var navigationBar: UIView!
    @IBOutlet weak var tblViewChat: UITableView!
    var dataSource: NSMutableArray = NSMutableArray()
    var interstitial: GADInterstitial!
    var is_ad_shown: Bool = false
  
    //MARK: -
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        navigationBar.backgroundColor = kNavColor
        
        interstitial = createAndLoadInterstitial()

        NotificationCenter.default.addObserver(self, selector: #selector(self.getInboxData), name: NSNotification.Name(rawValue: "GotoMessageVC"), object: nil)
        
        self.getInboxData()
//        self.GetSentRequestData()
//        self.GetUserRequestData()
    }
    
    //MARK:- Admob Operations
    
    func createAndLoadInterstitial() -> GADInterstitial {
        let interstitial = GADInterstitial(adUnitID: kAdInterstitialKey)
        interstitial.delegate = self
        
        interstitial.load(GADRequest())
        if interstitial.isReady {
            interstitial.present(fromRootViewController: self)
        } else {
            print("Ad wasn't ready")
        }
        return interstitial
    }
    
    /// Tells the delegate an ad request succeeded.
    func interstitialDidReceiveAd(_ ad: GADInterstitial) {
        print("interstitialDidReceiveAd")
        if is_ad_shown == false {
            ad.present(fromRootViewController: self)
            is_ad_shown = true
        }
    }
    
    /// Tells the delegate an ad request failed.
    func interstitial(_ ad: GADInterstitial, didFailToReceiveAdWithError error: GADRequestError) {
        print("interstitial:didFailToReceiveAdWithError: \(error.localizedDescription)")
    }
    
    /// Tells the delegate that an interstitial will be presented.
    func interstitialWillPresentScreen(_ ad: GADInterstitial) {
        print("interstitialWillPresentScreen")
    }
    
    /// Tells the delegate the interstitial is to be animated off the screen.
    func interstitialWillDismissScreen(_ ad: GADInterstitial) {
        print("interstitialWillDismissScreen")
    }
    
    /// Tells the delegate the interstitial had been animated off the screen.
    func interstitialDidDismissScreen(_ ad: GADInterstitial) {
        interstitial = createAndLoadInterstitial()
        print("interstitialDidDismissScreen")
    }
    
    /// Tells the delegate that a user click will open another app
    /// (such as the App Store), backgrounding the current app.
    func interstitialWillLeaveApplication(_ ad: GADInterstitial) {
        print("interstitialWillLeaveApplication")
    }
    
    //MARK: -
    func getInboxData() {
        
        let param = "{\"data\":{\"user_id\":\"\(userId)\"},\"function_name\":\"chatList\"}"
        APIManager.apiRequest(Parameters: param) { jsonResponse, error in
            
            if jsonResponse?.count != 0 {
                 print("chatList: ", jsonResponse!)
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
                if jsonResponse?.value(forKey: "msg_code") as? Bool == true {
                    self.dataSource = []
                    let data: NSArray = jsonResponse?.value(forKey: "data") as! NSArray
                    if data.count != 0{
                        self.dataSource.add(["chat": data as! [Any]])
                    }
                    self.GetSentRequestData()
//                    if self.dataSource.count == 3 {
//                        self.tblViewChat.reloadData()
//                    }
                } else {
                    if jsonResponse!.value(forKey: "message") != nil{
                        let msg : String = (jsonResponse!.value(forKey: "message") as? String)!
                        if(self.currentReachabilityStatus != .notReachable) {
                            self.showAlert(title: kError_Title, message: (jsonResponse!.value(forKey: "message") as? String)!)
                        } else {
                            self.showAlertInternet(title: kError_Title, message: msg, handler: {(alert: UIAlertAction!) in
                                self.getInboxData()
                            })
                        }
                    }
                }
            }
            return
        }
    }
    
    func GetSentRequestData() {
        
        let param = "{\"data\":{\"userid\":\"\(userId)\"},\"function_name\":\"sendrequestList\"}"
        
        APIManager.apiRequest(Parameters: param) { jsonResponse, error in
            if jsonResponse?.count != 0 {
                let msg : String = (jsonResponse!.value(forKey: "message") as? String)!
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
                if jsonResponse?.value(forKey: "msg_code") as? Bool == true {
                    let data: NSArray = jsonResponse?.value(forKey: "data") as! NSArray
                    if data.count != 0{
                        self.dataSource.add(["sent": data as! [Any]])
                    }
//                    if self.dataSource.count == 3 {
//                        self.tblViewChat.reloadData()
//                    }
                    self.GetUserRequestData()
                } else {
                    if(self.currentReachabilityStatus != .notReachable) {
                        self.showAlert(title: kError_Title, message: msg)
                    } else {
                        self.showAlertInternet(title: kError_Title, message: msg, handler: {(alert: UIAlertAction!) in
                            self.GetSentRequestData()
                        })
                    }
                }
            }
            return
        }
    }
    
    func GetUserRequestData() {
        
        let param = "{\"data\":{\"userid\":\"\(userId)\"},\"function_name\":\"userRequestlist\"}"
        
        APIManager.apiRequest(Parameters: param) { jsonResponse, error in
            if jsonResponse?.count != 0 {
                let msg : String = (jsonResponse!.value(forKey: "message") as? String)!
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
                if jsonResponse?.value(forKey: "msg_code") as? Bool == true {
                    let data: NSArray = jsonResponse?.value(forKey: "data") as! NSArray
                    if data.count != 0{
                        self.dataSource.add(["recieved": data as! [Any]])
                    }
//                    if self.dataSource.count == 3 {
                        self.tblViewChat.reloadData()
//                    }
                } else {
                    if(self.currentReachabilityStatus != .notReachable) {
                        self.showAlert(title: kError_Title, message: msg)
                    } else {
                        self.showAlertInternet(title: kError_Title, message: msg, handler: {(alert: UIAlertAction!) in
                            self.GetSentRequestData()
                        })
                    }
                }
            }
            return
        }
    }
    
    @IBAction func didTapBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

    @IBAction func goToMyProfile(_ sender: Any) {
        if SharedData.instance.selected_user_type == kCompany {
            let controller: CompanyProfileVC = self.storyboard?.instantiateViewController(withIdentifier: "CompanyProfileVC") as! CompanyProfileVC
            controller.user_Id = userId
            self.navigationController?.show(controller, sender: self)
        } else {
            let controller: InfluencerProfileVC = self.storyboard?.instantiateViewController(withIdentifier: "InfluencerProfileVC") as! InfluencerProfileVC
            controller.user_Id = userId
            self.navigationController?.show(controller, sender: self)
        }
    }
    
    //MARK: - Table View Delegates
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.dataSource.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ((self.dataSource[section] as! NSDictionary).allValues[0] as! NSArray).count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: InboxCell = tableView.dequeueReusableCell(withIdentifier: "InboxCell", for: indexPath) as! InboxCell
        if ((self.dataSource[indexPath.section] as! NSDictionary).allValues[0] as! NSArray).count != 0 {
           
            let item: NSDictionary = ((self.dataSource[indexPath.section] as! NSDictionary).allValues[0] as! NSArray)[indexPath.row] as! NSDictionary
            
            let keys: String = (self.dataSource[indexPath.section] as! NSDictionary).allKeys[0] as! String
            
            if keys == "chat" {
                cell.lblLastMsg.text = item.value(forKey: "message") as? String
                cell.lbltime.text = item.value(forKey: "added_date") as? String
            } else if keys == "recieved" {
                cell.lblLastMsg.text = "Request Recieved."
                cell.lbltime.text = item.value(forKey: "time") as? String
            } else {
                cell.lblLastMsg.text = "Request Sent."
                cell.lbltime.text = item.value(forKey: "time") as? String
            }

            if SharedData.instance.selected_user_type == kCompany {
                var fnm = ""
                if let fnm1: String = item.value(forKey: "fname") as? String {
                    fnm = fnm1
                    cell.lblUserName.text = fnm
                }
                if let lnm: String = item.value(forKey: "lname") as? String  {
                    cell.lblUserName.text = fnm + " " + lnm
                }
            } else {
                if let Cnm: String = item.value(forKey: "companyName") as? String{
                    cell.lblUserName.text = Cnm
                }
            }
            
            if let strUrl = item.value(forKey: "image") as? String {
                let url = URL(string: strUrl)
                cell.userImage.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "dot"), options: [.transition(.fade(1))], progressBlock: nil, completionHandler: nil)
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let controller: ChatVC = self.storyboard?.instantiateViewController(withIdentifier: "ChatVC") as! ChatVC
      
        let item: NSDictionary = ((self.dataSource[indexPath.section] as! NSDictionary).allValues[0] as! NSArray)[indexPath.row] as! NSDictionary
        
        var uid = ""
        let keys: String = (self.dataSource[indexPath.section] as! NSDictionary).allKeys[0] as! String
        
        if keys == "chat" {
            uid = (item.value(forKey: "user_id") as? String)!
        } else {
            uid = (item.value(forKey: "userId") as? String)!
        }
        if uid != "0" {
            controller.toUserId = uid
            if SharedData.instance.selected_user_type == kCompany {
                var fnm = ""
                if let fnm1 = item.value(forKey: "fname") as? String {
                    fnm = fnm1
                }
                if let lnm = item.value(forKey: "lname") as? String {
                    controller.toUserName = fnm + " " + lnm
                }
            } else {
                if let uimg = item.value(forKey: "image") as? String{
                    controller.toImage = uimg
                }
                if let Cnm = item.value(forKey: "companyName") as? String{
                    controller.toUserName = Cnm
                }
            }
            self.navigationController?.show(controller, sender: self)
        } else {
            self.showAlert(title: kError_Title, message: "User Id 0.")
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
}
