//
//  InboxCell.swift
//  WMA
//
//  Created by mac on 23/10/17.
//  Copyright © 2017 Impetrosys. All rights reserved.
//

import UIKit

class InboxCell: UITableViewCell {

    @IBOutlet weak var lblLastMsg: UILabel!
    @IBOutlet weak var lbltime: UILabel!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var btnGoToChat: UIButton!
    @IBOutlet weak var bubbleView: UIView!
    
    var borderWidth : CGFloat = 0.5 // Should be less or equal to the `radius` property
    var radius : CGFloat = 2
    var triangleHeight : CGFloat = 10
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func draw(_ rect: CGRect) {
        let bubbleLayer = CAShapeLayer()
        bubbleLayer.path = bubblePathForContentSize(contentSize: bubbleView.frame.size).cgPath
        bubbleLayer.fillColor = UIColor.clear.cgColor
        bubbleLayer.strokeColor = UIColor.lightGray.cgColor
        bubbleLayer.lineWidth = borderWidth
        bubbleLayer.position = CGPoint.zero
        bubbleView.layer.addSublayer(bubbleLayer)
    }

    private func bubblePathForContentSize(contentSize: CGSize) -> UIBezierPath {
        let rect = CGRect(x: 0, y: 0, width: contentSize.width, height: contentSize.height).offsetBy(dx: radius, dy: radius + triangleHeight)
        let path = UIBezierPath();
        let radius2 = radius - borderWidth / 2 // Radius adjasted for the border width
        
//        path.move(to: CGPoint(x: rect.maxX - triangleHeight * 2, y: rect.minY - radius2))
//        path.addLine(to: CGPoint(x: rect.maxX - triangleHeight, y: rect.minY - radius2 - triangleHeight))
        path.addArc(withCenter: CGPoint(x: rect.maxX, y: rect.minY),
                    radius: radius2,
                    startAngle: CGFloat(-(Double.pi/2)), endAngle: 0, clockwise: true)
        path.addArc(withCenter: CGPoint(x: rect.maxX, y: rect.maxY),
                    radius: radius2,
                    startAngle: 0, endAngle: CGFloat(Double.pi/2), clockwise: true)
        path.addArc(withCenter: CGPoint(x: rect.minX, y: rect.maxY),
                    radius: radius2,
                    startAngle: CGFloat(Double.pi/2),endAngle: CGFloat(Double.pi), clockwise: true)
        path.addArc(withCenter: CGPoint(x: rect.minX, y: rect.minY),
                    radius: radius2,
                    startAngle: CGFloat(Double.pi), endAngle: CGFloat(-(Double.pi/2)), clockwise: true)
        path.close()
        return path
    }

}
