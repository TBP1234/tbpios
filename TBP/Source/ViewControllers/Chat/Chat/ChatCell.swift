//
//  ChatCell.swift
//  WMA
//
//  Created by mac on 24/10/17.
//  Copyright © 2017 Impetrosys. All rights reserved.
//

import UIKit

class ChatCell: UITableViewCell {

    @IBOutlet weak var lblLastMsg: UILabel!
    @IBOutlet weak var lbltime: UILabel!
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var bubbleView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
