//
//  ChatVC.swift
//  WMA
//
//  Created by mac on 23/10/17.
//  Copyright © 2017 Impetrosys. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import Alamofire

class ChatVC: UIViewController, UITableViewDelegate, UITableViewDataSource, UIActionSheetDelegate {
    
    @IBOutlet weak var navigationBar: UIView!
    @IBOutlet weak var msgField: UITextView!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var tblViewChat: UITableView!
    
    @IBOutlet weak var btnSendReq: UIButton!
    @IBOutlet weak var imgSendReq: UIImageView!
    @IBOutlet weak var btnAcceptReq: UIButton!
    
    @IBOutlet weak var reqContainerView: UIView!
    @IBOutlet weak var sendReqView: UIView!
    @IBOutlet weak var pendingReqView: UIView!
    @IBOutlet weak var acceptView: UIView!
    @IBOutlet weak var btnBlock: UIButton!
    @IBOutlet weak var btnMenu: UIButton!
    
    var toUserId = ""
    var toUserName = ""
    var toImage = ""
    var dataDource: NSArray = NSArray()
    var blockstatus = "1"
    //MARK: -
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationBar.backgroundColor = kNavColor
        
        lblUserName.text = toUserName
        reqContainerView.isHidden = true
        sendReqView.isHidden = true
        pendingReqView.isHidden = true
        acceptView.isHidden = true
        btnBlock.isHidden = true
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.getChatData), name: NSNotification.Name(rawValue: "GotoMessageVC"), object: nil)
        self.getChatData()
    }

    @IBAction func didTapBack(_ sender: Any) {
        if notificationData != [:] {
            notificationData = [:]
        }
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func MenuTapped(_ sender: UIButton) {
       btnBlock.isHidden = false
    }
    
    @IBAction func blockUserTapped(_ sender: Any) {
        
        let param = "{\"data\":{\"userid_1\":\"\(userId)\",\"userid_2\":\"\(toUserId)\",\"from_block_userid\":\"\(userId)\",\"to_block_userid\":\"\(toUserId)\",\"block_status\":\"\(blockstatus)\"},\"function_name\":\"blockRequest\"}"
        APIManager.apiRequest(Parameters: param) { jsonResponse, error in
            print(jsonResponse!)
            self.btnBlock.isHidden = true
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            if jsonResponse != nil {
                if jsonResponse?.count != 0 {
                    let msg : String = (jsonResponse!.value(forKey: "message") as? String)!
                    if jsonResponse?.value(forKey: "msg_code") as? Bool == true {
                        if self.btnBlock.titleLabel?.text == "  Block"{
                            self.blockstatus = "0"
                            self.btnBlock.setTitle("  Unblock", for: .normal)
                        } else {
                            self.blockstatus = "1"
                            self.btnBlock.setTitle("  Block", for: .normal)
                        }
                        self.showAlert(title: msg, message: "")
                    } else {
                        if(self.currentReachabilityStatus != .notReachable) {
                            self.showAlert(title: msg, message: "")
                        } else {
                            self.showAlertInternet(title: kError_Title, message: msg, handler: {(alert: UIAlertAction!) in
                                self.IgnoreRequestTapped(self)
                            })
                        }
                    }
                }
            } else {
                self.showAlert(title: "JSON is not correct", message: "response is nil")
            }
            return
        }
    }
    
    func getChatData() {
      
        let param = "{\"data\":{\"user_id\":\"\(userId)\",\"user_id1\":\"\(toUserId)\"},\"function_name\":\"chat_history\"}"
        
        APIManager.apiRequest(Parameters: param) { jsonResponse, error in
            if jsonResponse?.count != 0 {
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
               
                if jsonResponse?.value(forKey: "fromblockid") != nil{
                    if (jsonResponse?.value(forKey: "fromblockid") as? String) != "0" {
                        if jsonResponse?.value(forKey: "fromblockid") as? String != userId {
                            self.btnMenu.isHidden = true
                        }
                        if jsonResponse?.value(forKey: "fromblockid") as? String == userId {
                            self.blockstatus = "0"
                            self.btnBlock.setTitle("  Unblock", for: .normal)
                        }
                    }
                }
                if jsonResponse?.value(forKey: "msg_code") as? Bool == true {
                    if (jsonResponse?.value(forKey: "message") as! String) == "Before send request then chat"{
                        self.reqContainerView.isHidden = false
                        self.sendReqView.isHidden = false
                        self.btnSendReq.backgroundColor = kNavColor
                        if SharedData.instance.selected_user_type == kCompany{
                            self.imgSendReq.image = #imageLiteral(resourceName: "send_request_a")
                        } else {
                            self.imgSendReq.image = #imageLiteral(resourceName: "send_request_b")
                        }
                    } else if (jsonResponse?.value(forKey: "message") as! String) == "Pendding request accept then chat"{
                        self.reqContainerView.isHidden = false
                        
                        let data = (jsonResponse?.value(forKey: "data") as! NSArray).object(at: 0)
                       
                        if userId == (data as AnyObject).value(forKey: "from_resquestid") as! String{
                            self.acceptView.isHidden = false
                            self.btnAcceptReq.backgroundColor = kNavColor
                            if SharedData.instance.selected_user_type == kCompany{
                                self.imgSendReq.image = #imageLiteral(resourceName: "request_accept_a")
                            } else {
                                self.imgSendReq.image = #imageLiteral(resourceName: "request_accept_b")
                            }
                        } else {
                            self.pendingReqView.isHidden = false
                            if SharedData.instance.selected_user_type == kCompany{
                                self.imgSendReq.image = #imageLiteral(resourceName: "invite_pending_a")
                            } else {
                                self.imgSendReq.image = #imageLiteral(resourceName: "invite_pending_b")
                            }
                            self.btnMenu.isHidden = true
                        }
                    } else {
                        if jsonResponse?.value(forKey: "data") != nil{
                            self.dataDource = (jsonResponse?.value(forKey: "data") as! NSArray).reversed() as NSArray
                            badgeCount = 0
                            if self.dataDource.count != 0{
                                self.tblViewChat.reloadData()
                            } else {
                                self.showAlert(title: (jsonResponse?.value(forKey: "message") as! String), message: "")
                            }
                        }
                        
                    }
                } else {
                    let msg : String = (jsonResponse!.value(forKey: "message") as? String)!
                    if(self.currentReachabilityStatus != .notReachable) {
                         self.showAlert(title: msg, message: "")
                    } else {
                        self.showAlertInternet(title: kError_Title, message: msg, handler: {(alert: UIAlertAction!) in
                            self.getChatData()
                        })
                    }
                }
                if self.dataDource.count != 0 {
                    let indexPath:IndexPath = IndexPath(row:(self.dataDource.count - 1), section:0)
                    self.tblViewChat.scrollToRow(at: indexPath, at: .bottom, animated: true)
                }
            }
            return
        }
    }
    
    func actionSheet(actionSheet: UIActionSheet, clickedButtonAtIndex buttonIndex: Int)
    {
        print("\(buttonIndex)")
        switch (buttonIndex){
            
        case 0:
            print("Cancel")
        case 1:
            print("Save")
        case 2:
            print("Delete")
        default:
            print("Default")
            //Some code here..
            
        }
    }
    
    //MARK: - Table View Delegates
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dataDource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell: ChatCell = ChatCell()
        let item: NSDictionary = self.dataDource[indexPath.row] as! NSDictionary
        
        let from_id = item.value(forKey: "from_id") as? String
        
        if from_id != userId {
            cell = tableView.dequeueReusableCell(withIdentifier: "ChatCellOther", for: indexPath) as! ChatCell
            cell.userImage.kf.setImage(with: URL(string: toImage), placeholder: #imageLiteral(resourceName: "dot"), options: [.transition(.fade(1))], progressBlock: nil, completionHandler: nil)
        } else {
            cell = tableView.dequeueReusableCell(withIdentifier: "ChatCellMe", for: indexPath) as! ChatCell
            cell.userImage.kf.setImage(with: URL(string: userImage), placeholder: #imageLiteral(resourceName: "dot"), options: [.transition(.fade(1))], progressBlock: nil, completionHandler: nil)
        }
      
        cell.bubbleView.addBorder(width: 0.5, color: .lightGray)
        if self.dataDource.count != 0 {
            cell.lblLastMsg.text = item.value(forKey: "message") as? String
            cell.lbltime.text = item.value(forKey: "added_date") as? String
        }
        return cell
    }
   
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        var hgtOfContent: CGFloat = 0.0
        let chatMsg = (self.dataDource[indexPath.row] as AnyObject).value(forKey: "message") as? String
        if chatMsg?.count != 0 {
            hgtOfContent = chatMsg!.heightForLabel()
        }
        
        if hgtOfContent < 120.0 {
            return 120.0
        } else {
            return hgtOfContent //+ 60.0
        }
    }
    
    //MARK: - Send Message
    @IBAction func sendRequestTapped(_ sender: Any) {
        let count = self.updateSentRequestCounter()
        if count <= 25 {
            let param = "{\"data\":{\"from_id\":\"\(userId)\",\"from_resquestid\":\"\(toUserId)\",\"to_id\":\"\(toUserId)\"},\"function_name\":\"sendRequest\"}"
            print(param)
            APIManager.apiRequest(Parameters: param) { jsonResponse, error in
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
                if jsonResponse != nil {
                    if jsonResponse?.count != 0 {
                        let msg : String = (jsonResponse!.value(forKey: "message") as? String)!
                        if jsonResponse?.value(forKey: "msg_code") as? Bool == true {
                            self.sendReqView.isHidden = true
                            self.pendingReqView.isHidden = false
                            self.btnMenu.isHidden = true
                            var msg2 = ""
                            if count > 20 {
                                msg2 = "You have \(25 - count) requests remaining, after 25 your account will be blocked."
                            }
                            self.showAlert(title: msg, message: msg2)
                        } else {
                            if(self.currentReachabilityStatus != .notReachable) {
                               self.showAlert(title: kError_Title, message: msg)
                            } else {
                                self.showAlertInternet(title: kError_Title, message: msg, handler: {(alert: UIAlertAction!) in
                                    self.sendRequestTapped(self)
                                })
                            }
                        }
                    }
                } else {
                    self.showAlert(title: "JSON is not correct", message: "response is nil")
                }
                return
            }
        } else {
            self.selfBlockUserOnExeedingLimit()
        }
    }
    
    func selfBlockUserOnExeedingLimit() {
        let param = "{\"data\":{\"userid\":\"\(userId)\"},\"function_name\":\"blockuser\"}"
        APIManager.apiRequest(Parameters: param) { jsonResponse, error in
           
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            if jsonResponse != nil {
                if jsonResponse?.count != 0 {
                    let msg : String = (jsonResponse!.value(forKey: "message") as? String)!
                    if jsonResponse?.value(forKey: "msg_code") as? Bool == true {
                         self.showAlert(title: "", message: "Your account has been blocked because the maximum number of request limit exceeded. Please contact admin for support.", handler: {(alert: UIAlertAction!) in
                            SharedData.PerformLogOutUser()
                        })
                    } else {
                        if(self.currentReachabilityStatus != .notReachable) {
                            self.showAlert(title: msg, message: "")
                        } else {
                            self.showAlertInternet(title: kError_Title, message: msg, handler: {(alert: UIAlertAction!) in
                                self.selfBlockUserOnExeedingLimit()
                            })
                        }
                    }
                }
            } else {
                self.showAlert(title: "JSON is not correct", message: "response is nil")
            }
            return
        }
    }
    
    func updateSentRequestCounter() -> Int{
        let sent_req_time_arr: NSMutableArray = NSMutableArray()
        if UserDefaults.standard.value(forKey: "sent_request_counter") != nil {
            let tempMuteArr: NSMutableArray = NSMutableArray()
            let temparr: NSArray = UserDefaults.standard.value(forKey: "sent_request_counter") as! NSArray
            tempMuteArr.addObjects(from: temparr as! [Any])
            print(tempMuteArr)
            for item in tempMuteArr {
                if (item as! Date) < Date().addingTimeInterval(-1800) {
                    tempMuteArr.remove(item) // Remove in this condition
                }
            }
            sent_req_time_arr.addObjects(from: tempMuteArr as! [Any])
        }
        sent_req_time_arr.add(Date())
        
        UserDefaults.standard.set(sent_req_time_arr, forKey: "sent_request_counter")
        UserDefaults.standard.synchronize()
        
        return sent_req_time_arr.count
    }
    
    @IBAction func AcceptRequestTapped(_ sender: Any) {
        let param = "{\"data\":{\"userid_1\":\"\(userId)\",\"userid_2\":\"\(toUserId)\"},\"function_name\":\"acceptRequest\"}"
        
        APIManager.apiRequest(Parameters: param) { jsonResponse, error in
            print(jsonResponse!)
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            if jsonResponse != nil {
                if jsonResponse?.count != 0 {
                    
                    let msg : String = (jsonResponse!.value(forKey: "message") as? String)!
                    if jsonResponse?.value(forKey: "msg_code") as? Bool == true {
                        self.showAlert(title: msg, message: "")
                        self.reqContainerView.isHidden = true
                    } else {
                        if(self.currentReachabilityStatus != .notReachable) {
                            self.showAlert(title: msg, message: "")
                        } else {
                            self.showAlertInternet(title: kError_Title, message: msg, handler: {(alert: UIAlertAction!) in
                                self.AcceptRequestTapped(self)
                            })
                        }
                    }
                }
            } else {
                self.showAlert(title: "JSON is not correct", message: "response is nil")
            }
            return
        }
    }
    
    @IBAction func IgnoreRequestTapped(_ sender: Any) {
        let param = "{\"data\":{\"userid_1\":\"\(userId)\",\"userid_2\":\"\(toUserId)\"},\"function_name\":\"cancelRequest\"}"
        
        APIManager.apiRequest(Parameters: param) { jsonResponse, error in
            print(jsonResponse!)
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            if jsonResponse != nil {
                if jsonResponse?.count != 0 {
                    let msg : String = (jsonResponse!.value(forKey: "message") as? String)!
                    if jsonResponse?.value(forKey: "msg_code") as? Bool == true {
                        self.showAlert(title: msg, message: "")
                        self.sendReqView.isHidden = false
                        self.pendingReqView.isHidden = true
                        self.acceptView.isHidden = true
                    } else {
                        if(self.currentReachabilityStatus != .notReachable) {
                            self.showAlert(title: msg, message: "")
                        } else {
                            self.showAlertInternet(title: kError_Title, message: msg, handler: {(alert: UIAlertAction!) in
                                self.IgnoreRequestTapped(self)
                            })
                        }
                    }
                }
            } else {
                self.showAlert(title: "JSON is not correct", message: "response is nil")
            }
            return
        }
    }
    
    //MARK: - Send Message
    
    @IBAction func sendButtonClicked(_ sender: AnyObject) {
        if !(msgField.text?.trim().isEmpty)!
        {
            if (self.msgField.text!.count != self.msgField.text!.components(separatedBy: "\n").count - 1) {
               
                let msgParam = ["message":"\(msgField.text!)"] as [String : Any]
                let str = String(describing: msgParam["message"]).replacingOccurrences(of: "Optional(\"", with: "").replacingOccurrences(of: "\")", with: "")
                
                let param = "{\"data\":{\"from_id\":\"\(userId)\",\"from_name\":\"\(userFullName)\",\"from_image\":\"\(userImage)\",\"to_id\":\"\(toUserId)\",\"message\":\"\(str)\"},\"function_name\":\"send_message\"}"
                
                APIManager.SendMessageRequest(Parameters: param) { jsonResponse, error in
//                    print(jsonResponse)
                    if jsonResponse != nil {
                        if jsonResponse?.count != 0 {
                            
                            let msg : String = (jsonResponse!.value(forKey: "message") as? String)!
                            if jsonResponse?.value(forKey: "msg_code") as? Bool == true {
                                self.getChatData()
                            } else {
                                if(self.currentReachabilityStatus != .notReachable) {
                                    if jsonResponse?.value(forKey: "blockid") != nil {
                                        
                                        if jsonResponse?.value(forKey: "blockid") as! String == userId {
                                            // create an actionSheet
                                            let actionSheetController: UIAlertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
                                            
                                            // create an action
                                            let firstAction: UIAlertAction = UIAlertAction(title: "Unblock", style: .default) { action -> Void in
                                                self.blockUserTapped(self)
                                            }
                                            
                                            let cancelAction: UIAlertAction = UIAlertAction(title: "Cancel", style: .cancel) { action -> Void in }
                                            
                                            // add actions
                                            actionSheetController.addAction(cancelAction)
                                            actionSheetController.addAction(firstAction)
                                            
                                            // present an actionSheet...
                                            self.present(actionSheetController, animated: true, completion: nil)
                                        } else {
                                            self.showAlert(title: msg, message: "")
                                        }
                                    } else {
                                        self.showAlert(title: msg, message: "")
                                    }
                                } else {
                                    self.showAlertInternet(title: kError_Title, message: msg, handler: {(alert: UIAlertAction!) in
                                        self.sendButtonClicked(self)
                                    })
                                }
                            }
                        }
                    } else {
                        self.showAlert(title: "JSON is not correct", message: "response is nil")
                    }
                return
                }
            self.msgField.text = ""
            self.msgField.resignFirstResponder()
        }
      }
    }
}
