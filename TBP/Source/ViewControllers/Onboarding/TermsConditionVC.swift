//
//  TermsConditionVC.swift
//  WMA
//
//  Created by mac on 08/11/17.
//  Copyright © 2017 Impetrosys. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
class TermsConditionVC: UIViewController, UIWebViewDelegate {

    @IBOutlet weak var webView: UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let urlpath = Bundle.main.path(forResource: "tc", ofType: "htm");
        let requesturl = URL(string: urlpath!)
        let request = URLRequest(url: requesturl!)
        webView.loadRequest(request)
    }
    
    @IBAction func didTapBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

    // MARK: - UIWebViewDelegate
   
    func webViewDidStartLoad(_ webView: UIWebView) {
       NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        webViewDidFinishLoad(webView)
    }

}
