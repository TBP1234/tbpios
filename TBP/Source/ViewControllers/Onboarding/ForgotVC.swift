//
//  ForgotVC.swift
//  WMA
//
//  Created by mac on 16/10/17.
//  Copyright © 2017 Impetrosys. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class ForgotVC: UIViewController {

    @IBOutlet weak var btnGo: UIButton!
    @IBOutlet weak var tf_emailToReset: UITextField!
    @IBOutlet weak var lblTitle: UILabel!
    
    var resetThing = "password"
    
    @IBOutlet weak var topConstraint: NSLayoutConstraint!
    //MARK: -
    
    override func viewDidLoad() {
        super.viewDidLoad()
        topConstraint.constant = CGFloat(navBarHeight)
        self.view.layoutIfNeeded()
        if SharedData.instance.selected_user_type == kCompany {
            btnGo.setBackgroundColor(kCompanyColor, for: .normal)
        } else {
            btnGo.setBackgroundColor(kInfluencerColor, for: .normal)
        }
        if resetThing == "password" {
            lblTitle.text = "Forgot Password"
        } else {
            lblTitle.text = "Forgot Username"
        }
    }

    @IBAction func didTapBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func ResetTapped(_ sender: Any) {
        tf_emailToReset.resignFirstResponder()
        if (tf_emailToReset.text?.trim().isEmpty)!
        {
            self.showAlert(title: kError_Title, message: kEmpty_Email)
        }
        else if(tf_emailToReset.text?.isValidEmail() == false) {
            self.showAlert(title: kError_Title, message:kInvalid_Email)
        } else{
            var param = ""
            if resetThing == "password" {
                param = "{\"data\":{\"email\":\"\(tf_emailToReset.text!)\"},\"function_name\":\"userforgot_password\"}"
            } else {
                param = "{\"data\":{\"email\":\"\(tf_emailToReset.text!)\"},\"function_name\":\"userforgot_username\"}"
            }
            
            APIManager.apiRequest(Parameters: param) { jsonResponse, error in
                if jsonResponse?.count != 0 {
//                    print(jsonResponse!)
                    
                    let msg : String = (jsonResponse!.value(forKey: "message") as? String)!
                    NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
                    if jsonResponse?.value(forKey: "msg_code") as? Bool == true {
                        self.showAlert(title: kSuccess, message: msg, handler: {(alert: UIAlertAction!) in
                            if self.resetThing == "password" {
                                let controller: ChangePasswordVC = self.storyboard?.instantiateViewController(withIdentifier: "ChangePasswordVC") as! ChangePasswordVC
                                controller.isForgot = true
                                controller.email = self.tf_emailToReset.text!
                                self.navigationController?.show(controller, sender: self)
                            } else {
                                self.didTapBack(self)
                            }
                        })
                    } else {
                        if(self.currentReachabilityStatus != .notReachable) {
                            self.showAlert(title: kError_Title, message: msg)
                        } else {
                            self.showAlertInternet(title: kError_Title, message: msg, handler: {(alert: UIAlertAction!) in
                                self.ResetTapped(self)
                            })
                        }
                    }
                }
                return
            }
            
        }
    }
    
    @IBAction func didTapToCreateNewAccount(_ sender: Any) {
        
        if (sender as AnyObject).tag == 1 {
            SharedData.instance.selected_user_type = kCompany
        } else {
            SharedData.instance.selected_user_type = kInfluencer
        }
         self.didTapBack(self)
    }

    
}
