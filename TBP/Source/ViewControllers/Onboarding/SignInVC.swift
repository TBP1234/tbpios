//
//  SignInVC.swift
//  TBP
//
//  Created by mac on 09/10/17.
//  Copyright © 2017 Impetrosys. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class SignInVC: UIViewController {

    @IBOutlet weak var tf_user_name: UITextField!
    @IBOutlet weak var tf_password: UITextField!
    
    @IBOutlet weak var btnRememberMe: UIButton!
    @IBOutlet weak var btnForgotUnm: UIButton!
    @IBOutlet weak var btnForgotPwd: UIButton!
    @IBOutlet weak var btnGo: UIButton!
  
    var bool_remember_me = true
    var imgTypeSelected = UIImage()
    var imgTypeDefault = UIImage()
    @IBOutlet weak var topConstraint: NSLayoutConstraint!
    
    //MARK: -
    
    override func viewDidLoad() {
        super.viewDidLoad()
        topConstraint.constant = CGFloat(navBarHeight)
        self.view.layoutIfNeeded()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.setDefaultDetails()
    }
    
    func setDefaultDetails() {
        if SharedData.instance.selected_user_type == kCompany {
            
//            tf_user_name.text = "com1"//"r_com"
//            tf_password.text = "q"//"1"
            
            btnGo.setBackgroundColor(kCompanyColor, for: .normal)
            btnForgotUnm.setTitleColor(kCompanyColor, for: .normal)
            btnForgotPwd.setTitleColor(kCompanyColor, for: .normal)
            
            imgTypeDefault = #imageLiteral(resourceName: "c_unchecked")
            imgTypeSelected = #imageLiteral(resourceName: "c_checked")
          
        } else {
            
//            tf_user_name.text = "inf1"//"r_inf"
//            tf_password.text = "q"//"1"
            
            btnGo.setBackgroundColor(kInfluencerColor, for: .normal)
            btnForgotUnm.setTitleColor(kInfluencerColor, for: .normal)
            btnForgotPwd.setTitleColor(kInfluencerColor, for: .normal)
            
            imgTypeDefault = #imageLiteral(resourceName: "i_unchecked")
            imgTypeSelected = #imageLiteral(resourceName: "i_checked")
        }
        btnRememberMe.setImage(imgTypeSelected, for: .normal)
    }
    
    @IBAction func didTapRememberMe(_ sender: Any) {
        if bool_remember_me == false {
            btnRememberMe.setImage(imgTypeSelected, for: .normal)
            bool_remember_me = true
        } else {
            btnRememberMe.setImage(imgTypeDefault, for: .normal)
            bool_remember_me = false
        }
    }

//MARK: - LOGIN
    
    @IBAction func didTapToLogin(_ sender: Any) {
        tf_password.resignFirstResponder()
        if (tf_user_name.text?.trim().isEmpty)!
        {
            self.showAlert(title: kError_Title, message: kUser_Name)
        }
        else if (tf_password.text?.trim().isEmpty)!
        {
            self.showAlert(title: kError_Title, message: kEmpty_Password)
        }
        else {
            handleSignIn()
        }
    }
    
    func handleSignIn() {
        
        let param = "{\"data\":{\"type\":\"\(SharedData.instance.selected_user_type)\",\"username\":\"\(tf_user_name.text!)\",\"password\":\"\(tf_password.text!)\",\"device_type\":\"ios\",\"device_token\":\"\(SharedData.instance.fireInstanceId)\"},\"function_name\":\"userLogin\"}"
      
//        print(param)
        
        APIManager.apiRequest(Parameters: param) { jsonResponse, error in
            
            if jsonResponse?.count != 0 {
    
                let msg : String = (jsonResponse!.value(forKey: "message") as? String)!
                 NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
                if jsonResponse?.value(forKey: "msg_code") as? Bool == true {
                    
                    self.tf_user_name.text = ""
                    self.tf_password.text = ""
                    if jsonResponse?.value(forKey: "data") != nil {
                        let data: NSArray = jsonResponse!.value(forKey: "data") as! NSArray
                        
                        let uid = (data[0] as AnyObject).value(forKey: "id") as! String
                        let email = (data[0] as AnyObject).value(forKey: "email") as! String
                        let active_status = (data[0] as AnyObject).value(forKey: "active_status") as! String
                        
                        var fullname = ""
                        
                        if SharedData.instance.selected_user_type == kCompany {
                            fullname = (data[0] as AnyObject).value(forKey: "companyName") as! String
                        } else {
                            let fnm = (data[0] as AnyObject).value(forKey: "firstname") as! String
                            let lnm = (data[0] as AnyObject).value(forKey: "lastName") as! String
                            fullname = fnm + " " + lnm
                        }
                        let image = (data[0] as AnyObject).value(forKey: "image") as! String
                        
                        let loginUserInfo = ["type": SharedData.instance.selected_user_type, "email" : email, "username" : self.tf_user_name.text!, "password": self.tf_password.text!, "user_id": "\(uid)", "fullName": fullname, "image": image] as [String : Any]
                       
                        if SharedData.instance.selected_user_type == kCompany{
                            if Int(active_status) == 1{
                                let controller: HomeCompanyVC = self.storyboard?.instantiateViewController(withIdentifier: "HomeCompanyVC") as! HomeCompanyVC
                                SharedData.instance.saveLoginUserInfoInLacal(loginUserInfo)
                                self.navigationController?.show(controller, sender: self)
                            } else {
                                let controller: CompleteCompanyProfileVC = self.storyboard?.instantiateViewController(withIdentifier: "CompleteCompanyProfileVC") as! CompleteCompanyProfileVC
                                user_login_info = loginUserInfo as NSDictionary
                                self.navigationController?.show(controller, sender: self)
                            }
                        } else {
                            if Int(active_status) == 1{
                                let controller: HomeInfluencerVC = self.storyboard?.instantiateViewController(withIdentifier: "HomeInfluencerVC") as! HomeInfluencerVC
                                SharedData.instance.saveLoginUserInfoInLacal(loginUserInfo)
                                self.navigationController?.show(controller, sender: self)
                            } else {
                                let controller: CompleteInfluencerProfileVC = self.storyboard?.instantiateViewController(withIdentifier: "CompleteInfluencerProfileVC") as! CompleteInfluencerProfileVC
                                user_login_info = loginUserInfo as NSDictionary
                                self.navigationController?.show(controller, sender: self)
                            }
                        }
                    }
                } else {
                    if(self.currentReachabilityStatus != .notReachable) {
                        self.showAlert(title: kError_Title, message: msg)
                    } else {
                        self.showAlertInternet(title: kError_Title, message: msg, handler: {(alert: UIAlertAction!) in
                            self.handleSignIn()
                        })
                    }
                }
            }
            return
        }
    }

   
//MARK: - RESET
    
    @IBAction func didTapToResetForgotThing(_ sender: UIButton) {
        
        let controller: ForgotVC = self.storyboard?.instantiateViewController(withIdentifier: "ForgotVC") as! ForgotVC
            if sender.tag == 1 {
                controller.resetThing = "password"
            } else {
                controller.resetThing = "username"
            }
        self.navigationController?.show(controller, sender: self)
    }
    
    
//MARK: - CREATE NEW ACCOUNT

    @IBAction func didTapToCreateNewAccount(_ sender: Any) {
        
        if (sender as AnyObject).tag == 1 {
            SharedData.instance.selected_user_type = kCompany
           
            let controller: RegistrationVC = self.storyboard?.instantiateViewController(withIdentifier: "RegistrationVC") as! RegistrationVC
            self.navigationController?.show(controller, sender: self)

        } else {
            SharedData.instance.selected_user_type = kInfluencer
            
            let controller: RegistrationVC = self.storyboard?.instantiateViewController(withIdentifier: "RegistrationVC") as! RegistrationVC
            self.navigationController?.show(controller, sender: self)
        }
    }
    
    
    @IBAction func didTapBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
