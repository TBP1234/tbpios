//
//  SelectUserTypeVC.swift
//  TBP
//
//  Created by mac on 09/10/17.
//  Copyright © 2017 Impetrosys. All rights reserved.
//

import UIKit

class SelectUserTypeVC: UIViewController {
    @IBOutlet weak var topConstraint: NSLayoutConstraint!
    
    //MARK: - 
    
    override func viewDidLoad() {
        super.viewDidLoad()
        topConstraint.constant = CGFloat(navBarHeight)
        self.view.layoutIfNeeded()
        // Do any additional setup after loading the view.
    }

    @IBAction func selectUserBtnClicked(_ sender: Any) {
        
        if (sender as AnyObject).tag == 1 {
            SharedData.instance.selected_user_type = kCompany
        } else {
            SharedData.instance.selected_user_type = kInfluencer
        }
        
        let controller: SignInVC = self.storyboard?.instantiateViewController(withIdentifier: "SignInVC") as! SignInVC
        self.navigationController?.show(controller, sender: self)
    }
    
    @IBAction func didTapBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}
