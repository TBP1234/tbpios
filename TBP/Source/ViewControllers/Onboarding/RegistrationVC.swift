//
//  RegistrationVC.swift
//  TBP
//
//  Created by mac on 09/10/17.
//  Copyright © 2017 Impetrosys. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class RegistrationVC: UIViewController {

    @IBOutlet weak var tf_user_name: UITextField!
    @IBOutlet weak var tf_password: UITextField!
    @IBOutlet weak var tf_Confirm_psw: UITextField!
    @IBOutlet weak var tf_email: UITextField!
    @IBOutlet weak var btnTerms: UIButton!
    @IBOutlet weak var btnRead: UIButton!
    @IBOutlet weak var btnGo: UIButton!
    @IBOutlet weak var navigationBar: UIView!
    @IBOutlet weak var lblInformation: UILabel!
    @IBOutlet weak var popUpView: UIView!
    @IBOutlet weak var btnGo2: UIButton!
    @IBOutlet weak var lblSucessMessage: UILabel!
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var containerView: UIView!
    
    var bool_accepted_TC = false
    
    var imgTypeSelected = UIImage()
    var imgTypeDefault = UIImage()
    
    @IBOutlet weak var topConstraint: NSLayoutConstraint!
    
    //MARK: -
    
    override func viewDidLoad() {
        super.viewDidLoad()
        topConstraint.constant = CGFloat(navBarHeight)
        self.view.layoutIfNeeded()
        
        containerView.isHidden = true
        
        if SharedData.instance.selected_user_type == kCompany {
            btnGo.setBackgroundColor(kCompanyColor, for: .normal)
            btnRead.setTitleColor(kCompanyColor, for: .normal)
            navigationBar.backgroundColor = kCompanyColor
            imgTypeDefault = #imageLiteral(resourceName: "c_unchecked")
            imgTypeSelected = #imageLiteral(resourceName: "c_checked")
            lblInformation.text = "Company Information"
            popUpView.backgroundColor = kCompanyColor
            btnGo2.setTitleColor(kCompanyColor, for: .normal)
            lblSucessMessage.text = "You have successfully created company"
            lblMessage.text = "Please complete your profile details on next screen."
        } else {
            btnGo.setBackgroundColor(kInfluencerColor, for: .normal)
            btnRead.setTitleColor(kInfluencerColor, for: .normal)
            navigationBar.backgroundColor = kInfluencerColor
            imgTypeDefault = #imageLiteral(resourceName: "i_unchecked")
            imgTypeSelected = #imageLiteral(resourceName: "i_checked")
            lblInformation.text = "Influencer Information"
            popUpView.backgroundColor = kInfluencerColor
            btnGo2.setTitleColor(kInfluencerColor, for: .normal)
            lblSucessMessage.text = "You have successfully created your account"
            lblMessage.text = "Please complete your profile details on next screen."//"You can use the app now to search the best companies to promote."
        }
        
        btnTerms.setImage(imgTypeDefault, for: .normal)
    }
    
    @IBAction func acceptTerms(_ sender: Any) {
        if bool_accepted_TC == false {
            btnTerms.setImage(imgTypeSelected, for: .normal)
            bool_accepted_TC = true
        } else {
            btnTerms.setImage(imgTypeDefault, for: .normal)
            bool_accepted_TC = false
        }
    }
    
    @IBAction func didTapToReadTerms(_ sender: Any) {
        let controller: TermsConditionVC = self.storyboard?.instantiateViewController(withIdentifier: "TermsConditionVC") as! TermsConditionVC
        self.navigationController?.show(controller, sender: self)

    }
    
    
    @IBAction func didTapToRegister(_ sender: Any) {
        if (tf_user_name.text?.trim().isEmpty)!
        {
            self.showAlert(title: kError_Title, message: kUser_Name)
        }
        else if (tf_password.text?.trim().isEmpty)!
        {
            self.showAlert(title: kError_Title, message: kEmpty_Password)
        }
//        else if (tf_password.text?.isvalidPassword() == false)
//        {
//            self.showAlert(title: kError_Title, message: kInvalid_Password)
//        }
        else if (tf_Confirm_psw.text?.trim().isEmpty)!
        {
            self.showAlert(title: kError_Title, message: kEmpty_C_Password)
        }
        else if (tf_password.text != tf_Confirm_psw.text)
        {
            self.showAlert(title: kError_Title, message: kConfirm_Password)
        }
        else if (tf_email.text?.trim().isEmpty)!
        {
            self.showAlert(title: kError_Title, message: kEmpty_Email)
        }
        else if(tf_email.text?.isValidEmail() == false) {
            self.showAlert(title: kError_Title, message:kInvalid_Email)
        }
        else if bool_accepted_TC == false{
            self.showAlert(title: kALERT, message: kTermsConditions)
        }
        else {
            handleRegistration()
        }
    }
    
    func handleRegistration() {
        tf_email.resignFirstResponder()
        let param = "{\"data\":{\"type\":\"\(SharedData.instance.selected_user_type)\",\"username\":\"\(tf_user_name.text!)\",\"email\":\"\(tf_email.text!)\",\"password\":\"\(tf_password.text!)\",\"device_type\":\"ios\",\"device_token\":\"\(SharedData.instance.fireInstanceId)\"},\"function_name\":\"creatProfile\"}"

        APIManager.apiRequest(Parameters: param) { jsonResponse, error in
            if jsonResponse?.count != 0 {
                
               // print(jsonResponse!)
                
                let msg : String = (jsonResponse!.value(forKey: "message") as? String)!
                 NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
                if jsonResponse?.value(forKey: "msg_code") as? Bool == true {
                    
                    
                    self.containerView.isHidden = false
                    user_login_info = ["type": SharedData.instance.selected_user_type, "email" : self.tf_email.text!, "username" : self.tf_user_name.text!, "password": self.tf_password.text!, "user_id": "\(jsonResponse?.value(forKey: "user_id") as! Int)"] as NSDictionary
                    
                } else {
                    if(self.currentReachabilityStatus != .notReachable) {
                        self.showAlert(title: kError_Title, message: msg)
                    } else {
                        self.showAlertInternet(title: kError_Title, message: msg, handler: {(alert: UIAlertAction!) in
                            self.handleRegistration()
                        })
                    }
                }
            }
            return
        }
    }
    
    @IBAction func popUpOkTapped(_ sender: Any) {
        
        if SharedData.instance.selected_user_type == kCompany{
            let controller: CompleteCompanyProfileVC = self.storyboard?.instantiateViewController(withIdentifier: "CompleteCompanyProfileVC") as! CompleteCompanyProfileVC
            controller.is_updating_profile = false
            self.navigationController?.show(controller, sender: self)
        } else {
            let controller: CompleteInfluencerProfileVC = self.storyboard?.instantiateViewController(withIdentifier: "CompleteInfluencerProfileVC") as! CompleteInfluencerProfileVC
            controller.is_updating_profile = false
            self.navigationController?.show(controller, sender: self)
        }
        self.tf_user_name.text = ""
        self.tf_password.text = ""
        self.tf_Confirm_psw.text = ""
        self.tf_email.text = ""
        self.containerView.isHidden = true
    }
    
    @IBAction func didTapBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}
