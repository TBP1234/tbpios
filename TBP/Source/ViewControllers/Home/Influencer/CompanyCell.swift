//
//  InfluencerCell.swift
//  TBP
//
//  Created by Ranjana Patidar on 15/10/17.
//  Copyright © 2017 Impetrosys. All rights reserved.
//

import UIKit

class CompanyCell: UITableViewCell {

    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblCountryCity: UILabel!
    @IBOutlet weak var lblProduct: UILabel!
     
    //MARK: -
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func configureCompanyList(_ userData: NSDictionary) {
        var fnm = ""
        var country = ""
        
        if (userData.value(forKey: "companyName")) != nil {
            fnm = userData.value(forKey: "companyName") as! String
            lblUserName.text = fnm
        }
        
        if (userData.value(forKey: "country")) != nil {
            country = userData.value(forKey: "country") as! String
            lblCountryCity.text = country
        }
        
        if (userData.value(forKey: "city")) != nil {
            let city = userData.value(forKey: "city") as! String
            lblCountryCity.text = country + ", " + city
        }
        
        if (userData.value(forKey: "typeOfProduct")) != nil {
            let typeOfProduct = userData.value(forKey: "typeOfProduct") as! String
            lblProduct.text = typeOfProduct
        }

    }
}
