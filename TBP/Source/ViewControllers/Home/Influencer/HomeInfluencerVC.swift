//
//  HomeVC.swift
//  TBP
//
//  Created by mac on 11/10/17.
//  Copyright © 2017 Impetrosys. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import GoogleMobileAds

class HomeInfluencerVC: UIViewController, UITableViewDelegate, UITableViewDataSource, GADInterstitialDelegate {

    @IBOutlet weak var navBar: UIView!
    @IBOutlet weak var back_btn_width: NSLayoutConstraint!
    @IBOutlet weak var TblVwCompanylist: UITableView!
    @IBOutlet weak var lblBadgeCount: UILabel!
    var dataSource: NSArray = NSArray()
    var interstitial: GADInterstitial!
    var is_ad_shown: Bool = false
    @IBOutlet weak var topConstraint: NSLayoutConstraint!
    //MARK: -
    
    override func viewDidLoad() {
        super.viewDidLoad()
        topConstraint.constant = CGFloat(navBarHeight)
        self.view.layoutIfNeeded()
        kNavColor = navBar.backgroundColor!
        if backBtnBool == false {
            back_btn_width.constant = 0
            self.hitApiToGetInfluencersData(self)
        } else {
            back_btn_width.constant = 44
        }
        self.view.layoutIfNeeded()
        
        if notificationData != [:] {
            let controller: ChatVC = self.storyboard?.instantiateViewController(withIdentifier: "ChatVC") as! ChatVC
            controller.toUserName = notificationData.value(forKey: "user_name") as! String
            controller.toUserId = notificationData.value(forKey: "user_id") as! String
            controller.toImage = notificationData.value(forKey: "image") as! String
            self.navigationController?.show(controller, sender: self)
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateBadgeCount), name: NSNotification.Name(rawValue: "updateBadgeCount"), object: nil)
         interstitial = createAndLoadInterstitial()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.updateBadgeCount()
    }
    
    func updateBadgeCount() {
        if badgeCount > 0 {
            lblBadgeCount.isHidden = false
            lblBadgeCount.text = String(badgeCount)
        } else {
            lblBadgeCount.isHidden = true
        }
    }
    
    //MARK:- Admob Operations
    
    func createAndLoadInterstitial() -> GADInterstitial {
        let interstitial = GADInterstitial(adUnitID: kAdInterstitialKey)
        interstitial.delegate = self
        
        interstitial.load(GADRequest())
        if interstitial.isReady {
            interstitial.present(fromRootViewController: self)
        } else {
            print("Ad wasn't ready")
        }
        return interstitial
    }
    
    /// Tells the delegate an ad request succeeded.
    func interstitialDidReceiveAd(_ ad: GADInterstitial) {
        print("interstitialDidReceiveAd")
        if is_ad_shown == false {
            ad.present(fromRootViewController: self)
            is_ad_shown = true
        }
    }
    
    /// Tells the delegate an ad request failed.
    func interstitial(_ ad: GADInterstitial, didFailToReceiveAdWithError error: GADRequestError) {
        print("interstitial:didFailToReceiveAdWithError: \(error.localizedDescription)")
    }
    
    /// Tells the delegate that an interstitial will be presented.
    func interstitialWillPresentScreen(_ ad: GADInterstitial) {
        print("interstitialWillPresentScreen")
    }
    
    /// Tells the delegate the interstitial is to be animated off the screen.
    func interstitialWillDismissScreen(_ ad: GADInterstitial) {
        print("interstitialWillDismissScreen")
    }
    
    /// Tells the delegate the interstitial had been animated off the screen.
    func interstitialDidDismissScreen(_ ad: GADInterstitial) {
        interstitial = createAndLoadInterstitial()
        print("interstitialDidDismissScreen")
    }
    
    /// Tells the delegate that a user click will open another app
    /// (such as the App Store), backgrounding the current app.
    func interstitialWillLeaveApplication(_ ad: GADInterstitial) {
        print("interstitialWillLeaveApplication")
    }
    
    //MARK:- Call API
    
    @IBAction func hitApiToGetInfluencersData(_ sender: Any) {
        let param = "{\"data\":{\"type\":\"\(kCompany)\"},\"function_name\":\"all_userList\"}"

//        print(param)
        APIManager.apiRequest(Parameters: param) { jsonResponse, error in
            if jsonResponse?.count != 0 {
                
                let msg : String = (jsonResponse!.value(forKey: "message") as? String)!
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
                if jsonResponse?.value(forKey: "msg_code") as? Bool == true {
                   if jsonResponse?.value(forKey: "data") != nil {
                    self.dataSource = (jsonResponse?.value(forKey: "data") as! NSArray).reversed() as NSArray
                    self.TblVwCompanylist.reloadData()
                    }
                } else {
                    if(self.currentReachabilityStatus != .notReachable) {
                        self.showAlert(title: kError_Title, message: msg)
                    } else {
                        self.showAlertInternet(title: kError_Title, message: msg, handler: {(alert: UIAlertAction!) in
                            self.hitApiToGetInfluencersData(self)
                        })
                    }
                }
            }
            return
        }
        
    }
    
    // MARK: - Table View Delgates and DataSource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: CompanyCell = tableView.dequeueReusableCell(withIdentifier: "CompanyCell", for: indexPath) as! CompanyCell
        if self.dataSource.count != 0 {
            cell.configureCompanyList(self.dataSource[indexPath.row] as! NSDictionary)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let controller: CompanyProfileVC = self.storyboard?.instantiateViewController(withIdentifier: "CompanyProfileVC") as! CompanyProfileVC
        controller.user_Id = (self.dataSource[indexPath.row] as! NSDictionary).value(forKey: "userId") as! String
        self.navigationController?.show(controller, sender: self)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }

    // MARK: -
    
    @IBAction func profileBtnTapped(_ sender: Any) {
        let controller: InfluencerProfileVC = self.storyboard?.instantiateViewController(withIdentifier: "InfluencerProfileVC") as! InfluencerProfileVC
        controller.user_Id = userId
        self.navigationController?.show(controller, sender: self)
    }
    
    @IBAction func chatButtonTapped(_ sender: Any) {
        let controller: InboxVC = self.storyboard?.instantiateViewController(withIdentifier: "InboxVC") as! InboxVC
        self.navigationController?.show(controller, sender: self)
    }
    
    @IBAction func didTapBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}
