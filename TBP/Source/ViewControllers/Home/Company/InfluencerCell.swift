//
//  InfluencerCell.swift
//  TBP
//
//  Created by Ranjana Patidar on 15/10/17.
//  Copyright © 2017 Impetrosys. All rights reserved.
//

import UIKit

class InfluencerCell: UITableViewCell {

    @IBOutlet weak var imgVwInfuencer: UIImageView!
    
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblCountryCity: UILabel!
    @IBOutlet weak var lblGender: UILabel!
    
    @IBOutlet weak var lblInstaFollowers: UILabel!
    @IBOutlet weak var lblFBFollowers: UILabel!
    @IBOutlet weak var lblSCFollowers: UILabel!
    
    var fb_profile_url = ""
    var is_profile_url = ""
    
    //MARK: -
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func configureInfuencerList(_ userData: NSDictionary) {
        var fnm = ""
        var country = ""
       
        if (userData.value(forKey: "image")) != nil {
            let url = URL(string: (userData.value(forKey: "image") as? String)!)
            self.imgVwInfuencer.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "download"), options: [.transition(.fade(1))], progressBlock: nil, completionHandler: nil)
        }
        
        if (userData.value(forKey: "firstname")) != nil {
            fnm = userData.value(forKey: "firstname") as! String
            lblUserName.text = fnm
        }
        if (userData.value(forKey: "lastName")) != nil  {
            let lnm = userData.value(forKey: "lastName") as! String
            lblUserName.text = fnm + " " + lnm
        }
        
        if (userData.value(forKey: "country")) != nil {
            country = userData.value(forKey: "country") as! String
            lblCountryCity.text = country
        }
        
        if (userData.value(forKey: "city")) != nil {
            let city = userData.value(forKey: "city") as! String
            lblCountryCity.text = country + ", " + city
        }
       
        if (userData.value(forKey: "instagram_followers")) != nil {
            let count = userData.value(forKey: "instagram_followers") as? String
            if count?.trim() != ""{
                lblInstaFollowers.text = Double(count!)?.suffixNumber()
            } else {
                lblSCFollowers.text = "NA"
            }
        }
        
        if (userData.value(forKey: "facebook_followers")) != nil {
            let count = userData.value(forKey: "facebook_followers") as? String
            if count?.trim() != ""{
                lblFBFollowers.text = Double(count!)?.suffixNumber()
            } else {
                lblSCFollowers.text = "NA"
            }
        }
        
        if (userData.value(forKey: "snapchat_followers")) != nil {
            let count = userData.value(forKey: "snapchat_followers") as? String
            if count?.trim() != ""{
                lblSCFollowers.text = Double(count!)?.suffixNumber()
            } else {
                lblSCFollowers.text = "NA"
            }
        }
        
        if (userData.value(forKey: "facebook_profile_url")) != nil {
            fb_profile_url = (userData.value(forKey: "facebook_profile_url") as? String)!
        }
        
        if (userData.value(forKey: "instagram_profile_url")) != nil {
            is_profile_url = (userData.value(forKey: "instagram_profile_url") as? String)!
        }
    }
    
    //MARK: - Visit Profile
    
    @IBAction func visitInstagramClicked(_ sender: Any) {
        if is_profile_url != "" {
            SharedData.openURLonWeb(URL(string: is_profile_url)!)
        }
    }
    
    @IBAction func visitFacebookClicked(_ sender: Any) {
        if fb_profile_url != "" {
            SharedData.openURLonWeb(URL(string: fb_profile_url)!)
        }
    }
    
    @IBAction func visitSnapChatClicked(_ sender: Any) {
    }
    
    
}
