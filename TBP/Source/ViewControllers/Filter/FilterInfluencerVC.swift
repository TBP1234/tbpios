//
//  FilterVC.swift
//  WMA
//
//  Created by mac on 06/11/17.
//  Copyright © 2017 Impetrosys. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import GoogleMobileAds

class FilterInfluencerVC: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate, GADInterstitialDelegate {

    @IBOutlet weak var tf_search: UITextField!
    @IBOutlet weak var tf_country: UITextField!
    @IBOutlet weak var tf_city: UITextField!
    
    @IBOutlet weak var btn_male: UIButton!
    @IBOutlet weak var btn_female: UIButton!
    @IBOutlet weak var btn_other: UIButton!
    
    @IBOutlet weak var tf_Insta_follower: UITextField!
    @IBOutlet weak var tf_fb_followers: UITextField!
    @IBOutlet weak var tf_Snap_followers: UITextField!
    
    var gender = ""
    
    @IBOutlet weak var containerViewCountry: UIView!
    @IBOutlet weak var countryTable: UITableView!
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var lblSelectorName: UILabel!
    @IBOutlet weak var lblBadgeCount: UILabel!
    
    var searchedCountryArray = [String]()
    var countriesArray = [String]()
    var countryCodeArr: NSMutableArray = NSMutableArray()
    
    var searchedCityArray = [String]()
    var citiesArray = [String]()
    var cityCodeArr: NSMutableArray = NSMutableArray()
    
    var selectedCountryCode = ""
    var changedCountryCode = ""
    var selectedCityCode = ""
    var activePicker = "country"
    var interstitial: GADInterstitial!
    var is_ad_shown: Bool = false
    
    @IBOutlet weak var topConstraint: NSLayoutConstraint!
    //MARK: -
    
    override func viewDidLoad() {
        super.viewDidLoad()
        topConstraint.constant = CGFloat(navBarHeight)
        self.view.layoutIfNeeded()
        backBtnBool = true
        containerViewCountry.isHidden = true
        
        interstitial = createAndLoadInterstitial()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateBadgeCount), name: NSNotification.Name(rawValue: "updateBadgeCount"), object: nil)
        
        self.getCountryList()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.updateBadgeCount()
    }
    
    func updateBadgeCount() {
        if badgeCount > 0 {
            lblBadgeCount.isHidden = false
            lblBadgeCount.text = String(badgeCount)
        } else {
            lblBadgeCount.isHidden = true
        }
    }
    
    //MARK:- Admob Operations
    
    func createAndLoadInterstitial() -> GADInterstitial {
        let interstitial = GADInterstitial(adUnitID: kAdInterstitialKey)
        interstitial.delegate = self
        
        interstitial.load(GADRequest())
        if interstitial.isReady {
            interstitial.present(fromRootViewController: self)
        } else {
            print("Ad wasn't ready")
        }
        return interstitial
    }
    
    /// Tells the delegate an ad request succeeded.
    func interstitialDidReceiveAd(_ ad: GADInterstitial) {
        print("interstitialDidReceiveAd")
        if is_ad_shown == false {
            ad.present(fromRootViewController: self)
            is_ad_shown = true
        }
    }
    
    /// Tells the delegate an ad request failed.
    func interstitial(_ ad: GADInterstitial, didFailToReceiveAdWithError error: GADRequestError) {
        print("interstitial:didFailToReceiveAdWithError: \(error.localizedDescription)")
    }
    
    /// Tells the delegate that an interstitial will be presented.
    func interstitialWillPresentScreen(_ ad: GADInterstitial) {
        print("interstitialWillPresentScreen")
    }
    
    /// Tells the delegate the interstitial is to be animated off the screen.
    func interstitialWillDismissScreen(_ ad: GADInterstitial) {
        print("interstitialWillDismissScreen")
    }
    
    /// Tells the delegate the interstitial had been animated off the screen.
    func interstitialDidDismissScreen(_ ad: GADInterstitial) {
        interstitial = createAndLoadInterstitial()
        print("interstitialDidDismissScreen")
    }
    
    /// Tells the delegate that a user click will open another app
    /// (such as the App Store), backgrounding the current app.
    func interstitialWillLeaveApplication(_ ad: GADInterstitial) {
        print("interstitialWillLeaveApplication")
    }
    
     //MARK: -
    @IBAction func filterInfluencers(_ sender: Any) {
        
        let insta_followers = Int(tf_Insta_follower.text!.components(separatedBy: "K")[0])! * 1000
        let fb_followers = Int(tf_fb_followers.text!.components(separatedBy: "K")[0])! * 1000
        let snap_followers = Int(tf_Snap_followers.text!.components(separatedBy: "K")[0])! * 1000
        
        let param = "{\"data\":{\"name\":\"\(tf_search.text!)\",\"country\":\"\(tf_country.text!)\",\"city\":\"\(tf_city.text!)\",\"branch\":\"\",\"gender\":\"\(gender)\",\"type\":\"\(kInfluencer)\",\"facebook_followers\":\"\(fb_followers)\",\"instagram_followers\":\"\(insta_followers)\",\"snapchat_followers\":\"\(snap_followers)\"},\"function_name\":\"findCompany\"}"
       
//        print(param)
        
        APIManager.apiRequest(Parameters: param) { jsonResponse, error in
            if jsonResponse?.count != 0 {
                
                let msg : String = (jsonResponse!.value(forKey: "message") as? String)!
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
                if jsonResponse?.value(forKey: "msg_code") as? Bool == true {
                    if jsonResponse?.value(forKey: "data") != nil {
                        let controller: HomeCompanyVC = self.storyboard?.instantiateViewController(withIdentifier: "HomeCompanyVC") as! HomeCompanyVC
                        controller.dataSource = (jsonResponse?.value(forKey: "data") as! NSArray).reversed() as NSArray
                        self.navigationController?.show(controller, sender: self)
                    }
                } else {
                    if(self.currentReachabilityStatus != .notReachable) {
                        self.showAlert(title: msg, message: "Please Try Again!")
                    } else {
                        self.showAlertInternet(title: kError_Title, message: msg, handler: {(alert: UIAlertAction!) in
                            self.filterInfluencers(self)
                        })
                    }
                }
            }
            return
        }
    }

    //MARK: -
    
    @IBAction func selectGender(_ sender: UIButton) {
        if sender.tag == 1 {
            gender = "male"
            btn_male.setImage(#imageLiteral(resourceName: "radio-orange"), for: .normal)
            btn_female.setImage(#imageLiteral(resourceName: "radio-white"), for: .normal)
            btn_other.setImage(#imageLiteral(resourceName: "radio-white"), for: .normal)
        }
        else if sender.tag == 2 {
            gender = "female"
            btn_female.setImage(#imageLiteral(resourceName: "radio-orange"), for: .normal)
            btn_male.setImage(#imageLiteral(resourceName: "radio-white"), for: .normal)
            btn_other.setImage(#imageLiteral(resourceName: "radio-white"), for: .normal)
        }
        else{
            gender = "other"
            btn_other.setImage(#imageLiteral(resourceName: "radio-orange"), for: .normal)
            btn_male.setImage(#imageLiteral(resourceName: "radio-white"), for: .normal)
            btn_female.setImage(#imageLiteral(resourceName: "radio-white"), for: .normal)
        }
    }
    
    @IBAction func increaseNumber(_ sender: UIButton) {
        if sender.tag == 11 {
            let num: Int = Int((tf_Insta_follower.text?.components(separatedBy: "K")[0])!)!
            tf_Insta_follower.text = "\(num + 1)K"
        }
        else if sender.tag == 12 {
            let num: Int = Int((tf_fb_followers.text?.components(separatedBy: "K")[0])!)!
            tf_fb_followers.text = "\(num + 1)K"
        }
        else{
            let num: Int = Int((tf_Snap_followers.text?.components(separatedBy: "K")[0])!)!
            tf_Snap_followers.text = "\(num + 1)K"
        }
    }
    
    @IBAction func decreaseNumber(_ sender: UIButton) {
        if sender.tag == 11 {
            let num: Int = Int((tf_Insta_follower.text?.components(separatedBy: "K")[0])!)!
            if num > 1 {
                tf_Insta_follower.text = "\(num - 1)K"
            } else {
                tf_Insta_follower.text = "0"
            }
        }
        else if sender.tag == 12 {
            let num: Int = Int((tf_fb_followers.text?.components(separatedBy: "K")[0])!)!
            if num > 1 {
                tf_fb_followers.text = "\(num - 1)K"
            } else {
                tf_fb_followers.text = "0"
            }
        }
        else{
            let num: Int = Int((tf_Snap_followers.text?.components(separatedBy: "K")[0])!)!
            if num > 1 {
                tf_Snap_followers.text = "\(num - 1)K"
            } else {
                tf_Snap_followers.text = "0"
            }
        }
    }
    
    //MARK: - Get Country and City list
    
    func getCountryList()
    {
        let param = "{\"function_name\":\"getCountries\"}"
        
        APIManager.apiRequest(Parameters: param) { jsonResponse, error in
            if jsonResponse?.count != 0 {
                
                let msg : String = (jsonResponse!.value(forKey: "message") as? String)!
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
                if jsonResponse?.value(forKey: "msg_code") as? Bool == true {
                    if jsonResponse?.value(forKey: "data") != nil {
                        let array = jsonResponse?.value(forKey: "data") as! NSArray
                        
                        let tempArr: NSMutableArray = NSMutableArray()
                        for item in array {
                            tempArr.add((item as AnyObject).value(forKey: "countryName")!)
                            self.countryCodeArr.add((item as AnyObject).value(forKey: "countryId")!)
                        }
                        self.countriesArray = tempArr.mutableCopy() as! [String]
                        self.searchedCountryArray = self.countriesArray
                        
                        self.countryTable.reloadData()
                        self.countryTable.setContentOffset(CGPoint.zero, animated: true)
                    }
                } else {
                    if(self.currentReachabilityStatus != .notReachable) {
                        self.showAlert(title: kError_Title, message: msg)
                    } else {
                        self.showAlertInternet(title: kError_Title, message: msg, handler: {(alert: UIAlertAction!) in
                            self.getCountryList()
                        })
                    }
                }
            }
            return
        }
    }
    
    func getCityList(countryId: String) {
        
        let param = "{\"data\":{\"countryId\":\"\(countryId)\"},\"function_name\":\"getCities\"}"
        
        APIManager.apiRequest(Parameters: param) { jsonResponse, error in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            if jsonResponse?.count != 0 {
                let msg : String = (jsonResponse!.value(forKey: "message") as? String)!
                if jsonResponse!.value(forKey: "msg_code") as! Bool == true{
                    if jsonResponse?.value(forKey: "data") != nil {
                        
                        let array = jsonResponse?.value(forKey: "data") as! NSArray
                        
                        let tempArr: NSMutableArray = NSMutableArray()
                        for item in array {
                            let strCityNm = (item as AnyObject).value(forKey: "cityName") as! String
                            tempArr.add(strCityNm)
                            self.cityCodeArr.add((item as AnyObject).value(forKey: "cityId")!)
                        }
                        self.citiesArray = tempArr.mutableCopy() as! [String]
                        self.searchedCityArray = self.citiesArray
                        
                        self.countryTable.reloadData()
                        self.countryTable.setContentOffset(CGPoint.zero, animated: true)
                        self.containerViewCountry.isHidden = false
                        self.lblSelectorName.text = "Select Your City"
                    }
                } else {
                    if(self.currentReachabilityStatus != .notReachable) {
                        self.showAlert(title: kError_Title, message: msg)
                    } else {
                        self.showAlertInternet(title: kError_Title, message: msg, handler: {(alert: UIAlertAction!) in
                            self.getCityList(countryId: countryId)
                        })
                    }
                }
            }
            return
        }
    }
    
    //MARK: - Table View Delegates
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if activePicker == "country" {
            return self.searchedCountryArray.count
        } else {
            return self.searchedCityArray.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        
        if activePicker == "country" {
            cell.textLabel?.text = self.searchedCountryArray[indexPath.row]
        } else {
            cell.textLabel?.text = self.searchedCityArray[indexPath.row]
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if activePicker == "country" {
            let strName = searchedCountryArray[indexPath.row] as String
            let index: Int = countriesArray.index(of: strName)!
            
            if selectedCountryCode == "" {
                selectedCountryCode = (countryCodeArr[index] as? String)!
                changedCountryCode = selectedCountryCode
            } else {
                selectedCountryCode = changedCountryCode
                changedCountryCode = (countryCodeArr[index] as? String)!
            }
            tf_country.text = strName
            tf_city.text = ""
            tf_country.resignFirstResponder()
        } else {
            let strName = searchedCityArray[indexPath.row] as String
            let index: Int = citiesArray.index(of: strName)!
            selectedCityCode = (cityCodeArr[index] as? String)!
            tf_city.text = strName
            tf_city.resignFirstResponder()
        }
        
        containerViewCountry.isHidden = true
        searchTextField.text = ""
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    // MARK: - TextField Delegates
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField == tf_country {
            activePicker = "country"
            if self.countriesArray.count != 0 {
                containerViewCountry.isHidden = false
                lblSelectorName.text = "Select Your Country"
                self.searchedCountryArray = self.countriesArray
                self.countryTable.reloadData()
                self.countryTable.setContentOffset(CGPoint.zero, animated: true)
            }
        }
        else if textField == tf_city {
            activePicker = "city"
            if selectedCountryCode != "" {
                if selectedCountryCode != changedCountryCode {
                    self.getCityList(countryId: changedCountryCode)
                } else {
                    if self.citiesArray.count != 0 {
                        self.searchedCityArray = self.citiesArray
                        self.countryTable.reloadData()
                        self.countryTable.setContentOffset(CGPoint.zero, animated: true)
                        self.containerViewCountry.isHidden = false
                        lblSelectorName.text = "Select Your City"
                    } else {
                        self.getCityList(countryId: changedCountryCode)
                    }
                }
            }
        }
    }
    
    @IBAction func searchRecordsAsPerText(_ sender: Any) {
        
        if activePicker == "country" {
            searchedCountryArray.removeAll()
            if searchTextField.text?.count != 0 {
                for strCountry in countriesArray {
                    let range = strCountry.lowercased().range(of: searchTextField.text!, options: .caseInsensitive, range: nil,   locale: nil)
                    
                    if range != nil {
                        searchedCountryArray.append(strCountry)
                    }
                }
            } else {
                searchedCountryArray = countriesArray
            }
        } else {
            searchedCityArray.removeAll()
            if searchTextField.text?.count != 0 {
                for strCountry in citiesArray {
                    let range = strCountry.lowercased().range(of: searchTextField.text!, options: .caseInsensitive, range: nil,   locale: nil)
                    
                    if range != nil {
                        searchedCityArray.append(strCountry)
                    }
                }
            } else {
                searchedCityArray = citiesArray
            }
        }
        countryTable.reloadData()
    }

    //MARK:- Go to my Profile
    
    @IBAction func profileBtnTapped(_ sender: Any) {
        let controller: CompanyProfileVC = self.storyboard?.instantiateViewController(withIdentifier: "CompanyProfileVC") as! CompanyProfileVC
        controller.user_Id = userId
        self.navigationController?.show(controller, sender: self)
    }
    
    @IBAction func chatButtonTapped(_ sender: Any) {
        let controller: InboxVC = self.storyboard?.instantiateViewController(withIdentifier: "InboxVC") as! InboxVC
        self.navigationController?.show(controller, sender: self)
    }
    
    @IBAction func didTapBack(_ sender: Any) {
        backBtnBool = false
        
//        let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
//        let reveal = (mainStoryBoard.instantiateViewController(withIdentifier: kCompany))
//        appDelegate.window?.rootViewController = reveal
        
        self.navigationController?.popViewController(animated: true)
//        self.navigationController?.popToRootViewController(animated: true)
    }
}
