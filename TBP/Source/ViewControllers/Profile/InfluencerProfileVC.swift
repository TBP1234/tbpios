//
//  InfluencerProfileVC.swift
//  TBP
//
//  Created by mac on 13/10/17.
//  Copyright © 2017 Impetrosys. All rights reserved.
//

import UIKit
import Kingfisher
import NVActivityIndicatorView
import GoogleMobileAds

class InfluencerProfileVC: UIViewController, GADInterstitialDelegate {

    @IBOutlet weak var lblViewInstAccount: UILabel!
    @IBOutlet weak var lblViewFBAccount: UILabel!
    @IBOutlet weak var lblViewSCAccount: UILabel!
    
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblCountryCity: UILabel!
    @IBOutlet weak var lblGender: UILabel!
    
    @IBOutlet weak var lblInstaFollowers: UILabel!
    @IBOutlet weak var lblFBFollowers: UILabel!
    @IBOutlet weak var lblSCFollowers: UILabel!

    @IBOutlet weak var lblAbout: UILabel!
    
    @IBOutlet weak var tf_InstaFollowers: UITextField!
    @IBOutlet weak var tf_FBFollowers: UITextField!
    @IBOutlet weak var tf_SCFollowers: UITextField!
  
    @IBOutlet weak var imgUser: UIImageView!
    
    @IBOutlet weak var btnchat: UIButton!
    @IBOutlet weak var btnChatWithMe: UIButton!
    @IBOutlet weak var btnLogout: UIButton!
    @IBOutlet weak var btnMyProfile: UIButton!
    @IBOutlet weak var btnChangePassword: UIButton!
    @IBOutlet weak var btnEdit: UIButton!
    @IBOutlet weak var tblProfile: UITableView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var viewLine: UIView!
    @IBOutlet weak var lblInfluProfile: UILabel!
    
    @IBOutlet weak var chatViewConstraint: NSLayoutConstraint!
    @IBOutlet weak var changePasswordConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var is_verified: UIButton!
    @IBOutlet weak var fb_verified: UIButton!
    @IBOutlet weak var sc_verified: UIButton!
    @IBOutlet weak var lblBadgeCount: UILabel!
    
    var fb_profile_url = ""
    var is_profile_url = ""
    
    var containerViewHgt: CGFloat = 0.0
    
    var userData: NSDictionary = NSDictionary()
    var user_Id = "0"
    var interstitial: GADInterstitial!
    var is_ad_shown: Bool = false
    
    @IBOutlet weak var topConstraint: NSLayoutConstraint!
    //MARK: -
    
    override func viewDidLoad() {
        super.viewDidLoad()
        topConstraint.constant = CGFloat(navBarHeight)
        self.view.layoutIfNeeded()
        self.prepareLayout()
        
        is_verified.isHidden = true
        fb_verified.isHidden = true
        sc_verified.isHidden = true
        
        self.tf_SCFollowers.isEnabled = false
        self.tf_FBFollowers.isEnabled = false
        self.tf_InstaFollowers.isEnabled = false

        interstitial = createAndLoadInterstitial()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        lblBadgeCount.isHidden = true
        if self.user_Id == userId {
            self.hitAPItoGetUserData()
        } else {
           self.updateBadgeCount()
            
            NotificationCenter.default.addObserver(self, selector: #selector(self.updateBadgeCount), name: NSNotification.Name(rawValue: "updateBadgeCount"), object: nil)
            
        }
    }

    func updateBadgeCount() {
        if badgeCount > 0 {
            lblBadgeCount.isHidden = false
            lblBadgeCount.text = String(badgeCount)
        }
    }
    func prepareLayout() {
        lblViewInstAccount.attributedText = self.changeToAtributedString("View INSTAGRAM Account", length: 9)
        lblViewFBAccount.attributedText = self.changeToAtributedString("View FACEBOOK Account", length: 8)
        lblViewSCAccount.attributedText = self.changeToAtributedString("View SNAPCHAT Account", length: 8)
        
        if self.user_Id == userId {
            containerViewHgt = 700.0
            self.setUpMyProfile()
        } else {
            containerViewHgt = 570.0
            self.setUpInfluencerProfile()
            self.hitAPItoGetUserData()
        }
    }
    
    func setUpMyProfile() {
        self.lblInfluProfile.text = "My Profile"
        self.btnMyProfile.isHidden = true
        self.btnchat.isHidden = true
    }
    
    func setUpInfluencerProfile() {

        self.btnLogout.isHidden = true
        self.btnChangePassword.isHidden = true
        self.btnEdit.isHidden = true
        changePasswordConstraint.constant = 0
        
        self.view.layoutIfNeeded()
    }
    
    //MARK:- Admob Operations
    
    func createAndLoadInterstitial() -> GADInterstitial {
        let interstitial = GADInterstitial(adUnitID: kAdInterstitialKey)
        interstitial.delegate = self
        
        interstitial.load(GADRequest())
        if interstitial.isReady {
            interstitial.present(fromRootViewController: self)
        } else {
            print("Ad wasn't ready")
        }
        return interstitial
    }
    
    /// Tells the delegate an ad request succeeded.
    func interstitialDidReceiveAd(_ ad: GADInterstitial) {
        print("interstitialDidReceiveAd")
        if is_ad_shown == false {
            ad.present(fromRootViewController: self)
            is_ad_shown = true
        }
    }
    
    /// Tells the delegate an ad request failed.
    func interstitial(_ ad: GADInterstitial, didFailToReceiveAdWithError error: GADRequestError) {
        print("interstitial:didFailToReceiveAdWithError: \(error.localizedDescription)")
    }
    
    /// Tells the delegate that an interstitial will be presented.
    func interstitialWillPresentScreen(_ ad: GADInterstitial) {
        print("interstitialWillPresentScreen")
    }
    
    /// Tells the delegate the interstitial is to be animated off the screen.
    func interstitialWillDismissScreen(_ ad: GADInterstitial) {
        print("interstitialWillDismissScreen")
    }
    
    /// Tells the delegate the interstitial had been animated off the screen.
    func interstitialDidDismissScreen(_ ad: GADInterstitial) {
        interstitial = createAndLoadInterstitial()
        print("interstitialDidDismissScreen")
    }
    
    /// Tells the delegate that a user click will open another app
    /// (such as the App Store), backgrounding the current app.
    func interstitialWillLeaveApplication(_ ad: GADInterstitial) {
        print("interstitialWillLeaveApplication")
    }
    
    //MARK:- Call API
    
    func hitAPItoGetUserData() {
        
        let param = "{\"data\":{\"userId\":\"\(self.user_Id)\"},\"function_name\":\"userDetail\"}"
        
      //  print(param)
        
        APIManager.apiRequest(Parameters: param) { jsonResponse, error in
            if jsonResponse?.count != 0 {
                
              //  print(jsonResponse!)
                
                let msg : String = (jsonResponse!.value(forKey: "message") as? String)!
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
                if jsonResponse?.value(forKey: "msg_code") as? Bool == true {
                    
                    var fnm = ""
                    var country = ""
                    if jsonResponse?.value(forKey: "data") != nil {
                        let dataTemp: NSArray = jsonResponse?.value(forKey: "data") as! NSArray
                        self.userData = dataTemp[0] as! NSDictionary
                        
//                        print(self.userData)
                        
                        if (self.userData.value(forKey: "image")) != nil {
                            let url = URL(string: (self.userData.value(forKey: "image") as? String)!)
                            self.imgUser.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "download"), options: [.transition(.fade(1))], progressBlock: nil, completionHandler: nil)
                        }
                        
                        if (self.userData.value(forKey: "firstname")) != nil {
                            fnm = self.userData.value(forKey: "firstname") as! String
                            self.lblUserName.text = fnm
                        }
                        if (self.userData.value(forKey: "lastName")) != nil  {
                            let lnm = self.userData.value(forKey: "lastName") as! String
                            self.lblUserName.text = fnm + " " + lnm
                        }
                        
                        if (self.userData.value(forKey: "country")) != nil {
                            country = self.userData.value(forKey: "country") as! String
                            self.lblCountryCity.text = country
                        }
                        
                        if (self.userData.value(forKey: "city")) != nil {
                            let city = self.userData.value(forKey: "city") as! String
                            self.lblCountryCity.text = country + ", " + city
                        }
                        
                        if (self.userData.value(forKey: "gender")) != nil {
                            let gender = self.userData.value(forKey: "gender") as! String
                            self.lblGender.text = "Gender: " + gender
                        }
                        
                        if self.userData.value(forKey: "about") != nil {
                            let aboutText = self.userData.value(forKey: "about") as? String
                            self.lblAbout.text = aboutText
                            self.lblAbout.sizeToFit()
                            let hgtOfAbout = self.lblAbout.frame.height + 40
                            
                            self.containerView.frame.size = CGSize(width: self.tblProfile.frame.width, height: CGFloat(self.containerViewHgt + hgtOfAbout + 22))
                            self.tblProfile.contentSize = self.containerView.frame.size
                            self.containerView.layoutIfNeeded()
                        }
                        
                        if (self.userData.value(forKey: "instagram_followers")) != nil {
                            let count = self.userData.value(forKey: "instagram_followers") as? String
                            if count != "" {
                                self.tf_InstaFollowers.text = Double(count!)?.suffixNumber()
                                self.lblInstaFollowers.text = Double(count!)?.suffixNumber()
                                self.is_verified.isHidden = false
                                if (self.userData.value(forKey: "instagram_profile_url")) != nil {
                                    self.is_profile_url = (self.userData.value(forKey: "instagram_profile_url") as? String)!
                                }
                            }
                        }
                        
                        if (self.userData.value(forKey: "facebook_followers")) != nil {
                            let count = self.userData.value(forKey: "facebook_followers") as? String
                            if count != "" {
                                self.tf_FBFollowers.text = Double(count!)?.suffixNumber()
                                self.lblFBFollowers.text = Double(count!)?.suffixNumber()
                                self.fb_verified.isHidden = false
                                if (self.userData.value(forKey: "facebook_profile_url")) != nil {
                                    self.fb_profile_url = (self.userData.value(forKey: "facebook_profile_url") as? String)!
                                }
                            }
                        }
                        
                        if (self.userData.value(forKey: "snapchat_followers")) != nil {
                            let count = self.userData.value(forKey: "snapchat_followers") as? String
                            if count != "" {
                                self.tf_SCFollowers.text = Double(count!)?.suffixNumber()
                                self.lblSCFollowers.text = Double(count!)?.suffixNumber()
                            }
                        }
                    }
                } else {
                    if(self.currentReachabilityStatus != .notReachable) {
                        self.showAlert(title: kError_Title, message: msg)
                    } else {
                        self.showAlertInternet(title: kError_Title, message: msg, handler: {(alert: UIAlertAction!) in
                            self.hitAPItoGetUserData()
                        })
                    }
                }
            }
            return
        }
    }
    
    func changeToAtributedString(_ text: String, length: Int) -> NSAttributedString {
        let SCMutableString = NSMutableAttributedString(string: text)
        SCMutableString.setAttributes([NSFontAttributeName : UIFont(name: "Futura", size: CGFloat(12.0))!
            , NSForegroundColorAttributeName : kInfluencerColor], range: NSRange(location:5,length:length))
        return SCMutableString
    }
    
     //MARK: - Visit Profile
    
    @IBAction func visitInstagramClicked(_ sender: Any) {
        if self.is_verified.isHidden == false {
            if is_profile_url != "" {
                SharedData.openURLonWeb(URL(string: is_profile_url)!)
            }
        }
    }
    
    @IBAction func visitFacebookClicked(_ sender: Any) {
        if self.fb_verified.isHidden == false {
            if fb_profile_url != "" {
        
                SharedData.openURLonWeb(URL(string: fb_profile_url)!)
            }
        }
    }

    @IBAction func visitSnapChatClicked(_ sender: Any) {
    }
    
     //MARK: - Button Actions
    
    @IBAction func chatWithMeTapped(_ sender: Any) {
        
        if self.user_Id == userId {
            let controller: InboxVC = self.storyboard?.instantiateViewController(withIdentifier: "InboxVC") as! InboxVC
            self.navigationController?.show(controller, sender: self)
        } else {
            let controller: ChatVC = self.storyboard?.instantiateViewController(withIdentifier: "ChatVC") as! ChatVC
            
            let fnm = userData.value(forKey: "firstname") as! String
            let lnm = userData.value(forKey: "lastName") as! String
            
            controller.toUserId = self.user_Id
            controller.toUserName = fnm + " " + lnm
            controller.toImage = userData.value(forKey: "image") as! String
            self.navigationController?.show(controller, sender: self)
        }
    }
    
    @IBAction func logoutTapped(_ sender: Any) {
        
        let uiAlert = UIAlertController(title: kALERT, message: kLogOut_Message, preferredStyle: UIAlertControllerStyle.alert)
        self.present(uiAlert, animated: true, completion: nil)
        
        uiAlert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
            if UserDefaults.standard.value(forKey: kUserLoginInfo) != nil{
                SharedData.PerformLogOutUser()
            }
        }))
        
        uiAlert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { action in
            print("Click of cancel button")
        }))
    }
    
    @IBAction func goToEditProfile(_ sender: Any) {
        let controller: CompleteInfluencerProfileVC = self.storyboard?.instantiateViewController(withIdentifier: "CompleteInfluencerProfileVC") as! CompleteInfluencerProfileVC
        controller.is_updating_profile = true
        controller.userData = userData
        self.navigationController?.show(controller, sender: self)
    }
    
    @IBAction func goToChangePassword(_ sender: Any) {
        let controller: ChangePasswordVC = self.storyboard?.instantiateViewController(withIdentifier: "ChangePasswordVC") as! ChangePasswordVC
        SharedData.instance.selected_user_type = kInfluencer
        self.navigationController?.show(controller, sender: self)
    }

    @IBAction func didTapBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func goToMyProfile(_ sender: Any) {
        if self.user_Id == userId {
            
        } else {
            let controller: CompanyProfileVC = self.storyboard?.instantiateViewController(withIdentifier: "CompanyProfileVC") as! CompanyProfileVC
            controller.user_Id = userId
            self.navigationController?.show(controller, sender: self)
        }
    }
    
    @IBAction func chatButtonTapped(_ sender: Any) {
        let controller: InboxVC = self.storyboard?.instantiateViewController(withIdentifier: "InboxVC") as! InboxVC
        self.navigationController?.show(controller, sender: self)
    }
}
