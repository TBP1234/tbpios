//
//  CompanyProfileVC.swift
//  TBP
//
//  Created by mac on 13/10/17.
//  Copyright © 2017 Impetrosys. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import GoogleMobileAds

class CompanyProfileVC: UIViewController, GADInterstitialDelegate {

    @IBOutlet weak var lblUserFullName: UILabel!
    @IBOutlet weak var lblUserCountryCity: UILabel!
    @IBOutlet weak var lblBranch: UILabel!
    @IBOutlet weak var lblAboutUs: UILabel!
    @IBOutlet weak var lblContactEmail: UILabel!
    @IBOutlet weak var lblContactName: UILabel!
    
    @IBOutlet weak var btnchat: UIButton!
    @IBOutlet weak var btnChatWithMe: UIButton!
    @IBOutlet weak var btnLogout: UIButton!
    @IBOutlet weak var btnMyProfile: UIButton!
    @IBOutlet weak var btnChangePassword: UIButton!
    @IBOutlet weak var btnEdit: UIButton!
    @IBOutlet weak var tblProfile: UITableView!
    @IBOutlet weak var containerView: UIView!
    
    @IBOutlet weak var lblCompanyProfile: UILabel!
    @IBOutlet weak var chatViewConstraint: NSLayoutConstraint!
    @IBOutlet weak var changePasswordConstraint: NSLayoutConstraint!
    @IBOutlet weak var lblBadgeCount: UILabel!
    
    var containerViewHgt: CGFloat = 0.0
   
    var userData: NSDictionary = NSDictionary()
    var user_Id = "0"
    var interstitial: GADInterstitial!
    var is_ad_shown: Bool = false
    @IBOutlet weak var topConstraint: NSLayoutConstraint!
    //MARK: -
    
    override func viewDidLoad() {
        super.viewDidLoad()
        topConstraint.constant = CGFloat(navBarHeight)
        self.view.layoutIfNeeded()
        self.prepareLayout()
        
        interstitial = createAndLoadInterstitial()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        lblBadgeCount.isHidden = true
        if self.user_Id == userId {
             self.hitAPItoGetUserData()
        } else {
            self.updateBadgeCount()
            
            NotificationCenter.default.addObserver(self, selector: #selector(self.updateBadgeCount), name: NSNotification.Name(rawValue: "updateBadgeCount"), object: nil)
            
        }
    }
  
    func updateBadgeCount() {
        if badgeCount > 0 {
            lblBadgeCount.isHidden = false
            lblBadgeCount.text = String(badgeCount)
        }
    }
    
    func prepareLayout() {
         if self.user_Id == userId {
            containerViewHgt = 460.0
            self.setUpMyProfile()
        } else {
            containerViewHgt = 330.0
            self.setUpInfluencerProfile()
            self.hitAPItoGetUserData()
        }
    }
    
    func setUpMyProfile() {
        self.lblCompanyProfile.text = "My Profile"
        self.btnMyProfile.isHidden = true
        self.btnchat.isHidden = true
    }
    
    func setUpInfluencerProfile() {
        self.btnLogout.isHidden = true
        self.btnChangePassword.isHidden = true
        self.btnEdit.isHidden = true
        changePasswordConstraint.constant = 0
        self.view.layoutIfNeeded()
    }
    
    
    //MARK:- Admob Operations
    
    func createAndLoadInterstitial() -> GADInterstitial {
        let interstitial = GADInterstitial(adUnitID: kAdInterstitialKey)
        interstitial.delegate = self
        
        interstitial.load(GADRequest())
        if interstitial.isReady {
            interstitial.present(fromRootViewController: self)
        } else {
            print("Ad wasn't ready")
        }
        return interstitial
    }
    
    /// Tells the delegate an ad request succeeded.
    func interstitialDidReceiveAd(_ ad: GADInterstitial) {
        print("interstitialDidReceiveAd")
        if is_ad_shown == false {
            ad.present(fromRootViewController: self)
            is_ad_shown = true
        }
    }
    
    /// Tells the delegate an ad request failed.
    func interstitial(_ ad: GADInterstitial, didFailToReceiveAdWithError error: GADRequestError) {
        print("interstitial:didFailToReceiveAdWithError: \(error.localizedDescription)")
    }
    
    /// Tells the delegate that an interstitial will be presented.
    func interstitialWillPresentScreen(_ ad: GADInterstitial) {
        print("interstitialWillPresentScreen")
    }
    
    /// Tells the delegate the interstitial is to be animated off the screen.
    func interstitialWillDismissScreen(_ ad: GADInterstitial) {
        print("interstitialWillDismissScreen")
    }
    
    /// Tells the delegate the interstitial had been animated off the screen.
    func interstitialDidDismissScreen(_ ad: GADInterstitial) {
        interstitial = createAndLoadInterstitial()
        print("interstitialDidDismissScreen")
    }
    
    /// Tells the delegate that a user click will open another app
    /// (such as the App Store), backgrounding the current app.
    func interstitialWillLeaveApplication(_ ad: GADInterstitial) {
        print("interstitialWillLeaveApplication")
    }
    
    //MARK:- Call API
    
    func hitAPItoGetUserData() {
        
        let param = "{\"data\":{\"userId\":\"\(self.user_Id)\"},\"function_name\":\"userDetail\"}"
        
       // print(param)
        
        APIManager.apiRequest(Parameters: param) { jsonResponse, error in
            if jsonResponse?.count != 0 {
                
               // print(jsonResponse!)
                
                let msg : String = (jsonResponse!.value(forKey: "message") as? String)!
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
                if jsonResponse?.value(forKey: "msg_code") as? Bool == true {
                    
                    var fnm = ""
                    var country = ""
                    if jsonResponse?.value(forKey: "data") != nil {
                        let dataTemp: NSArray = jsonResponse?.value(forKey: "data") as! NSArray
                        self.userData = dataTemp[0] as! NSDictionary
                    }
                    
                    if self.userData.value(forKey: "companyName") != nil {
                        self.lblUserFullName.text = self.userData.value(forKey: "companyName") as? String
                    }
                    
                    if (self.userData.value(forKey: "country")) != nil {
                        country = self.userData.value(forKey: "country") as! String
                        self.lblBranch.text = country
                    }

                    if (self.userData.value(forKey: "city")) != nil {
                        let city = self.userData.value(forKey: "city") as! String
                        self.lblBranch.text = country + ", " + city
                    }

                    if (self.userData.value(forKey: "typeOfProduct")) != nil {
                        let branch = self.userData.value(forKey: "typeOfProduct") as! String
                        self.lblUserCountryCity.text = branch
                    }
                    
                    if self.userData.value(forKey: "about") != nil {
                        let aboutText = self.userData.value(forKey: "about") as? String
                        self.lblAboutUs.text = aboutText
                        self.lblAboutUs.sizeToFit()
                        let hgtOfAbout = self.lblAboutUs.frame.height
                        
                        self.containerView.frame.size = CGSize(width: self.tblProfile.frame.width, height: CGFloat(self.containerViewHgt + hgtOfAbout + 22))
                        
                        self.tblProfile.contentSize = self.containerView.frame.size
                        
                        self.containerView.layoutIfNeeded()
                    }
                    
                    if self.userData.value(forKey: "email") != nil {
                        self.lblContactEmail.text = self.userData.value(forKey: "email") as? String
                    }
                    
                    if (self.userData.value(forKey: "firstname")) != nil {
                        fnm = self.userData.value(forKey: "firstname") as! String
                        self.lblContactName.text = fnm
                    }
                    if (self.userData.value(forKey: "lastName")) != nil  {
                        let lnm = self.userData.value(forKey: "lastName") as! String
                        self.lblContactName.text = fnm + " " + lnm
                    }
                    
                } else {
                    if(self.currentReachabilityStatus != .notReachable) {
                        self.showAlert(title: kError_Title, message: msg)
                    } else {
                        self.showAlertInternet(title: kError_Title, message: msg, handler: {(alert: UIAlertAction!) in
                            self.hitAPItoGetUserData()
                        })
                    }
                }
            }
            return
        }
    }
   
    //MARK: - Button Actions
    
    @IBAction func didTapBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func profileBtnTapped(_ sender: Any) {
        if self.user_Id == userId {
            
        } else {
            let controller: InfluencerProfileVC = self.storyboard?.instantiateViewController(withIdentifier: "InfluencerProfileVC") as! InfluencerProfileVC
            controller.user_Id = userId
            self.navigationController?.show(controller, sender: self)
        }
    }
    
    @IBAction func chatButtonTapped(_ sender: Any) {
        let controller: InboxVC = self.storyboard?.instantiateViewController(withIdentifier: "InboxVC") as! InboxVC
        self.navigationController?.show(controller, sender: self)
    }
    
    @IBAction func goToEditProfile(_ sender: Any) {
        let controller: CompleteCompanyProfileVC = self.storyboard?.instantiateViewController(withIdentifier: "CompleteCompanyProfileVC") as! CompleteCompanyProfileVC
        controller.is_updating_profile = true
        controller.userData = userData
        self.navigationController?.show(controller, sender: self)
    }
    
    @IBAction func goToChangePassword(_ sender: Any) {
        let controller: ChangePasswordVC = self.storyboard?.instantiateViewController(withIdentifier: "ChangePasswordVC") as! ChangePasswordVC
        SharedData.instance.selected_user_type = kCompany
        self.navigationController?.show(controller, sender: self)
    }
    
    @IBAction func logoutTapped(_ sender: Any) {
        let uiAlert = UIAlertController(title: kALERT, message: kLogOut_Message, preferredStyle: UIAlertControllerStyle.alert)
        self.present(uiAlert, animated: true, completion: nil)
        
        uiAlert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
            if UserDefaults.standard.value(forKey: kUserLoginInfo) != nil{
                SharedData.PerformLogOutUser()
            }
        }))
        
        uiAlert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { action in
            print("Click of cancel button")
        }))
    }
    
    @IBAction func chatWithMeTapped(_ sender: Any) {
        if self.user_Id == userId {
            let controller: InboxVC = self.storyboard?.instantiateViewController(withIdentifier: "InboxVC") as! InboxVC
            self.navigationController?.show(controller, sender: self)
        } else {
            let controller: ChatVC = self.storyboard?.instantiateViewController(withIdentifier: "ChatVC") as! ChatVC
            controller.toUserId = self.user_Id
            controller.toUserName = userData.value(forKey: "companyName") as! String
            controller.toImage = userData.value(forKey: "image") as! String
            self.navigationController?.show(controller, sender: self)
        }
    }

}
