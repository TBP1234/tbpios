//
//  ChangePasswordVC.swift
//  TBP
//
//  Created by Ranjana Patidar on 15/10/17.
//  Copyright © 2017 Impetrosys. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class ChangePasswordVC: UIViewController {

    @IBOutlet weak var tf_old_psw: UITextField!
    @IBOutlet weak var tf_password: UITextField!
    @IBOutlet weak var tf_Confirm_psw: UITextField!
    
    @IBOutlet weak var btnGo: UIButton!
    @IBOutlet weak var navigationBar: UIView!
    
    @IBOutlet weak var lblOTP: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblBadgeCount: UILabel!
    
    var isForgot = false
    var email = ""
    @IBOutlet weak var topConstraint: NSLayoutConstraint!
    //MARK: -
    
    override func viewDidLoad() {
        super.viewDidLoad()
        topConstraint.constant = CGFloat(navBarHeight)
        self.view.layoutIfNeeded()
        print(SharedData.instance.selected_user_type)
        
        if isForgot == true {
            if SharedData.instance.selected_user_type == kCompany {
                btnGo.setBackgroundColor(kCompanyColor, for: .normal)
                navigationBar.backgroundColor = kCompanyColor
            } else {
                btnGo.setBackgroundColor(kCompanyColor, for: .normal)
                navigationBar.backgroundColor = kInfluencerColor
            }
            lblOTP.text = "OTP"
            tf_old_psw.placeholder = "OTP"
            lblTitle.text = "Reset Password"
            btnGo.setTitle("Reset Password", for: .normal)
        } else {
            lblOTP.text = "Old Password"
            tf_old_psw.placeholder = "Old Password"
            btnGo.setBackgroundColor(kNavColor, for: .normal)
            btnGo.setTitle("Change Password", for: .normal)
            lblTitle.text = "Change Password"
            navigationBar.backgroundColor = kNavColor
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateBadgeCount), name: NSNotification.Name(rawValue: "updateBadgeCount"), object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.updateBadgeCount()
    }
    
    func updateBadgeCount() {
        if badgeCount > 0 {
            lblBadgeCount.isHidden = false
            lblBadgeCount.text = String(badgeCount)
        } else {
            lblBadgeCount.isHidden = true
        }
    }
    
    @IBAction func didTapBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func changePasswordTapped(_ sender: Any) {
        if (tf_old_psw.text?.trim().isEmpty)!
        {
            if isForgot == true {
                self.showAlert(title: kError_Title, message: "Please enter your OTP, which is sent to your email.")
            } else {
                self.showAlert(title: kError_Title, message: kEmpty_Old_Password)
            }
        }
        else if (tf_password.text?.trim().isEmpty)!
        {
            self.showAlert(title: kError_Title, message: kEmpty_Password)
        }
//        else if (tf_password.text?.isvalidPassword() == false)
//        {
//            self.showAlert(title: kError_Title, message: kInvalid_Password)
//        }
        else if (tf_Confirm_psw.text?.trim().isEmpty)!
        {
            self.showAlert(title: kError_Title, message: kEmpty_C_Password)
        }
        else if (tf_password.text != tf_Confirm_psw.text)
        {
            self.showAlert(title: kError_Title, message: kConfirm_Password)
        }
        else{
            if isForgot == true{
                self.performResetForgotPassword()
            } else {
                self.performChangePassword()
            }
        }
    }
    
    func performChangePassword() {
        
        let param = "{\"data\":{\"userId\":\"\(userId)\",\"password\":\"\(tf_password.text!)\"},\"function_name\":\"userchangePassword\"}"
        
        print(param)
        
        APIManager.apiRequest(Parameters: param) { jsonResponse, error in
            if jsonResponse?.count != 0 {
                
                //                    print(jsonResponse!)
                
                let msg : String = (jsonResponse!.value(forKey: "message") as? String)!
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
                if jsonResponse?.value(forKey: "msg_code") as? Bool == true {
                    
                    self.showAlert(title: kSuccess, message: msg, handler: {(alert: UIAlertAction!) in
                        self.didTapBack(self)
                    })
                } else {
                    
                    self.showAlertInternet(title: kError_Title, message: msg, handler: {(alert: UIAlertAction!) in
                        self.changePasswordTapped(self)
                    })
                    //                        self.showAlert(title: kError_Title, message: msg)
                }
            }
            return
        }
    }
    
    func performResetForgotPassword() {
        
        let param = "{\"data\":{\"email\":\"\(email)\",\"password\":\"\(tf_password.text!)\",\"otp\":\"\(tf_old_psw.text!)\"},\"function_name\":\"resetPassword\"}"
        
        print(param)
        
        APIManager.apiRequest(Parameters: param) { jsonResponse, error in
            if jsonResponse?.count != 0 {
                
            print(jsonResponse!)
                
                let msg : String = (jsonResponse!.value(forKey: "message") as? String)!
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
                if jsonResponse?.value(forKey: "msg_code") as? Bool == true {
                    
                    self.showAlert(title: kSuccess, message: msg, handler: {(alert: UIAlertAction!) in
                        self.navigationController?.popToRootViewController(animated: true)
                    })
                } else {
                    
                    self.showAlertInternet(title: kError_Title, message: msg, handler: {(alert: UIAlertAction!) in
                        self.changePasswordTapped(self)
                    })
                    //                        self.showAlert(title: kError_Title, message: msg)
                }
            }
            return
        }
    }
    
    
    @IBAction func chatButtonTapped(_ sender: Any) {
        let controller: InboxVC = self.storyboard?.instantiateViewController(withIdentifier: "InboxVC") as! InboxVC
        self.navigationController?.show(controller, sender: self)
    }
    
}
