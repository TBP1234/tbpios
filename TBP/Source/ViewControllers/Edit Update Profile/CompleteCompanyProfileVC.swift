//
//  UpdateCompanyProfileVC.swift
//  TBP
//
//  Created by mac on 10/10/17.
//  Copyright © 2017 Impetrosys. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class CompleteCompanyProfileVC: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate {
    
    @IBOutlet weak var tf_first_name: UITextField!
    @IBOutlet weak var tf_last_name: UITextField!
    @IBOutlet weak var tf_company_name: UITextField!
    @IBOutlet weak var tf_product_type: UITextField!
    @IBOutlet weak var tf_about: UITextView!
    @IBOutlet weak var tf_country: UITextField!
    @IBOutlet weak var tf_city: UITextField!
    @IBOutlet weak var tf_branch: UITextField!
    
    @IBOutlet weak var btnSaveUpdate: UIButton!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var containerView: UIView!
    
    var is_updating_profile = false
    var userData : NSDictionary = NSDictionary()
    
    @IBOutlet weak var containerViewCountry: UIView!
    @IBOutlet weak var countryTable: UITableView!
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var lblSelectorName: UILabel!
    
    var searchedCountryArray = [String]()
    var countriesArray = [String]()
    var countryCodeArr: NSMutableArray = NSMutableArray()
    
    var searchedCityArray = [String]()
    var citiesArray = [String]()
    var cityCodeArr: NSMutableArray = NSMutableArray()
    
    var selectedCountryCode = ""
    var changedCountryCode = ""
    var selectedCityCode = ""
    var activePicker = "country"
    
    @IBOutlet weak var topConstraint: NSLayoutConstraint!
    
    //MARK: -
    
    override func viewDidLoad() {
        super.viewDidLoad()
        topConstraint.constant = CGFloat(navBarHeight)
        self.view.layoutIfNeeded()
        
        containerView.isHidden = true
        containerViewCountry.isHidden = true
        self.getCountryList()
       
        if is_updating_profile == true {
            self.parseUserData()
        }
    }
    
    func parseUserData() {
        
        if (self.userData.value(forKey: "firstname")) != nil {
            self.tf_first_name.text = self.userData.value(forKey: "firstname") as? String
        }
        
        if (self.userData.value(forKey: "lastName")) != nil  {
            self.tf_last_name.text = self.userData.value(forKey: "lastName") as? String
        }
        
        if (self.userData.value(forKey: "companyName")) != nil {
            self.tf_company_name.text = self.userData.value(forKey: "companyName") as? String
        }
        
        if self.userData.value(forKey: "typeOfProduct") != nil {
            self.tf_product_type.text = self.userData.value(forKey: "typeOfProduct") as? String
        }
        
        if (self.userData.value(forKey: "about")) != nil {
            self.tf_about.text = self.userData.value(forKey: "about") as? String
        }
        
        if (self.userData.value(forKey: "branch")) != nil {
            self.tf_branch.text = self.userData.value(forKey: "branch") as? String
        }
        
        if (self.userData.value(forKey: "country")) != nil {
            self.tf_country.text = self.userData.value(forKey: "country") as? String
            if (self.userData.value(forKey: "countryCode")) != nil {
               selectedCountryCode = self.userData.value(forKey: "countryCode") as! String
               changedCountryCode = selectedCountryCode
            }
        }
        
        if (self.userData.value(forKey: "city")) != nil {
            self.tf_city.text = self.userData.value(forKey: "city") as? String
            if (self.userData.value(forKey: "cityCode")) != nil {
                selectedCityCode = self.userData.value(forKey: "cityCode") as! String
            }
        }
    
        lblTitle.text = "Edit Profile"
        btnSaveUpdate.setTitle("UPDATE", for: .normal)
    }

    
    //MARK: - Get Country and City list
    
    func getCountryList()
    {
        let param = "{\"function_name\":\"getCountries\"}"
        
        APIManager.apiRequest(Parameters: param) { jsonResponse, error in
            if jsonResponse?.count != 0 {
                
                let msg : String = (jsonResponse!.value(forKey: "message") as? String)!
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
                if jsonResponse?.value(forKey: "msg_code") as? Bool == true {
                    if jsonResponse?.value(forKey: "data") != nil {
                        let array = jsonResponse?.value(forKey: "data") as! NSArray
                        
                        let tempArr: NSMutableArray = NSMutableArray()
                        for item in array {
                            tempArr.add((item as AnyObject).value(forKey: "countryName")!)
                            self.countryCodeArr.add((item as AnyObject).value(forKey: "countryId")!)
                        }
                        self.countriesArray = tempArr.mutableCopy() as! [String]
                        self.searchedCountryArray = self.countriesArray
                        
                        self.countryTable.reloadData()
                        self.countryTable.setContentOffset(CGPoint.zero, animated: true)
                    }
                } else {
                    if(self.currentReachabilityStatus != .notReachable) {
                        self.showAlert(title: kError_Title, message: msg)
                    } else {
                        self.showAlertInternet(title: kError_Title, message: msg, handler: {(alert: UIAlertAction!) in
                            self.getCountryList()
                        })
                    }
                }
                
            }
            return
        }
    }
    
    func getCityList(countryId: String) {
        
        let param = "{\"data\":{\"countryId\":\"\(countryId)\"},\"function_name\":\"getCities\"}"
       
        APIManager.apiRequest(Parameters: param) { jsonResponse, error in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            if jsonResponse?.count != 0 {
                let msg : String = (jsonResponse!.value(forKey: "message") as? String)!
                if jsonResponse!.value(forKey: "msg_code") as! Bool == true{
                    if jsonResponse?.value(forKey: "data") != nil {
                        
                        let array = jsonResponse?.value(forKey: "data") as! NSArray
                        
                        let tempArr: NSMutableArray = NSMutableArray()
                        for item in array {
                            let strCityNm = (item as AnyObject).value(forKey: "cityName") as! String
                            tempArr.add(strCityNm)
                            self.cityCodeArr.add((item as AnyObject).value(forKey: "cityId")!)
                        }
                        self.citiesArray = tempArr.mutableCopy() as! [String]
                        self.searchedCityArray = self.citiesArray
                        
                        self.countryTable.reloadData()
                        self.countryTable.setContentOffset(CGPoint.zero, animated: true)
                        self.containerViewCountry.isHidden = false
                        self.lblSelectorName.text = "Select Your City"
                    }
                } else {
                    if(self.currentReachabilityStatus != .notReachable) {
                        self.showAlert(title: kError_Title, message: msg)
                    } else {
                        self.showAlertInternet(title: kError_Title, message: msg, handler: {(alert: UIAlertAction!) in
                            self.getCityList(countryId: countryId)
                        })
                    }
                }
            }
            return
        }
    }
    
    //MARK: - Table View Delegates
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if activePicker == "country" {
            return self.searchedCountryArray.count
        } else {
            return self.searchedCityArray.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        
        if activePicker == "country" {
            cell.textLabel?.text = self.searchedCountryArray[indexPath.row]
        } else {
            cell.textLabel?.text = self.searchedCityArray[indexPath.row]
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if activePicker == "country" {
            let strName = searchedCountryArray[indexPath.row] as String
            let index: Int = countriesArray.index(of: strName)!
            
            if selectedCountryCode == "" {
                selectedCountryCode = (countryCodeArr[index] as? String)!
                changedCountryCode = selectedCountryCode
            } else {
                selectedCountryCode = changedCountryCode
                changedCountryCode = (countryCodeArr[index] as? String)!
            }
            tf_country.text = strName
            tf_city.text = ""
            tf_country.resignFirstResponder()
        } else {
            let strName = searchedCityArray[indexPath.row] as String
            let index: Int = citiesArray.index(of: strName)!
            selectedCityCode = (cityCodeArr[index] as? String)!
            tf_city.text = strName
            tf_city.resignFirstResponder()
        }
        
        containerViewCountry.isHidden = true
        searchTextField.text = ""
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    // MARK: - TextField Delegates
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField == tf_country {
            activePicker = "country"
            if self.countriesArray.count != 0 {
                containerViewCountry.isHidden = false
                lblSelectorName.text = "Select Your Country"
                self.searchedCountryArray = self.countriesArray
                self.countryTable.reloadData()
                self.countryTable.setContentOffset(CGPoint.zero, animated: true)
            }
        }
        else if textField == tf_city {
            activePicker = "city"
            if selectedCountryCode != "" {
                if selectedCountryCode != changedCountryCode {
                    self.getCityList(countryId: changedCountryCode)
                } else {
                    if self.citiesArray.count != 0 {
                        self.searchedCityArray = self.citiesArray
                        self.countryTable.reloadData()
                        self.countryTable.setContentOffset(CGPoint.zero, animated: true)
                        self.containerViewCountry.isHidden = false
                        lblSelectorName.text = "Select Your City"
                    } else {
                        self.getCityList(countryId: changedCountryCode)
                    }
                }
            }
            tf_city.resignFirstResponder()
        }
    }
    
    @IBAction func searchRecordsAsPerText(_ sender: Any) {
        
        if activePicker == "country" {
            searchedCountryArray.removeAll()
            if searchTextField.text?.count != 0 {
                for strCountry in countriesArray {
                    let range = strCountry.lowercased().range(of: searchTextField.text!, options: .caseInsensitive, range: nil,   locale: nil)
                    
                    if range != nil {
                        searchedCountryArray.append(strCountry)
                    }
                }
            } else {
                searchedCountryArray = countriesArray
            }
        } else {
            searchedCityArray.removeAll()
            if searchTextField.text?.count != 0 {
                for strCountry in citiesArray {
                    let range = strCountry.lowercased().range(of: searchTextField.text!, options: .caseInsensitive, range: nil,   locale: nil)
                    
                    if range != nil {
                        searchedCityArray.append(strCountry)
                    }
                }
            } else {
                searchedCityArray = citiesArray
            }
        }
        countryTable.reloadData()
    }
    

    
  //MARK: - Save Company Details

    @IBAction func saveCompanyDetails(_ sender: Any) {
       
        if (tf_first_name.text?.trim().isEmpty)!
        {
            self.showAlert(title: kError_Title, message: kFirst_Name)
        }
        else if (tf_last_name.text?.trim().isEmpty)!
        {
            self.showAlert(title: kError_Title, message: kLast_Name)
        }
        else if (tf_company_name.text?.trim().isEmpty)!
        {
            self.showAlert(title: kError_Title, message: kCompany_Name)
        }
        else if (tf_product_type.text?.trim().isEmpty)!
        {
            self.showAlert(title: kError_Title, message: kProduct_Type)
        }
        else if (tf_about.text?.trim().isEmpty)!
        {
            self.showAlert(title: kError_Title, message: kAbout)
        }
        else if (tf_branch.text?.trim().isEmpty)!
        {
            self.showAlert(title: kError_Title, message: kBranch_Name)
        }
        else if (tf_country.text?.trim().isEmpty)!
        {
            self.showAlert(title: kError_Title, message: kCountry_URL)
        }
        else if (tf_city.text?.trim().isEmpty)!
        {
            self.showAlert(title: kError_Title, message: kCity_URL)
        }
        else {
            
            if is_updating_profile == true {
                self.updateProfile()
            } else {
                self.performCompleteProfile()
            }
        }
    }
    
    func updateProfile()  {
        let image = ""
        
        let msgParam = ["message":"\(tf_about.text!)"] as [String : Any]
        let str = String(describing: msgParam["message"]).replacingOccurrences(of: "Optional(\"", with: "").replacingOccurrences(of: "\")", with: "")
        
        let param = "{\"data\":{\"userId\":\"\(userId)\",\"about\":\"\(str)\",\"branch\":\"\(tf_branch.text!)\",\"name\":\"\(tf_first_name.text!)\",\"lastName\":\"\(tf_last_name.text!)\",\"companyName\":\"\(tf_company_name.text!)\",\"image\":\"\(image)\",\"typeOfProduct\":\"\(tf_product_type.text!)\",\"country\":\"\(tf_country.text!)\",\"city\":\"\(tf_city.text!)\",\"cityCode\":\"\(selectedCityCode)\",\"countryCode\":\"\(selectedCountryCode)\"},\"function_name\":\"edit_companyUser\"}"
        
        APIManager.apiRequest(Parameters: param) { jsonResponse, error in
            if jsonResponse?.count != 0 {
                
//                print(jsonResponse!)
                
                let msg : String = (jsonResponse!.value(forKey: "message") as? String)!
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
                if jsonResponse?.value(forKey: "msg_code") as? Bool == true {
                    self.showAlert(title: kSuccess, message: msg, handler: {(alert: UIAlertAction!) in
                        self.didTapBack(self)
                    })
                    
                } else {
                    if(self.currentReachabilityStatus != .notReachable) {
                        self.showAlert(title: kError_Title, message: msg)
                    } else {
                        self.showAlertInternet(title: kError_Title, message: msg, handler: {(alert: UIAlertAction!) in
                            self.updateProfile()
                        })
                    }
                }
            }
            return
        }
    }

    func performCompleteProfile() {
        
        let username = user_login_info.value(forKey: "username") as! String
        let email = user_login_info.value(forKey: "email") as! String
        let password = user_login_info.value(forKey: "password") as! String
        let uid = user_login_info.value(forKey: "user_id") as! String
        let image = ""
        
        let msgParam = ["message":"\(tf_about.text!)"] as [String : Any]
        let str = String(describing: msgParam["message"]).replacingOccurrences(of: "Optional(\"", with: "").replacingOccurrences(of: "\")", with: "")
        
        let param = "{\"data\":{\"userId\":\"\(uid)\",\"countryCode\":\"\(selectedCountryCode)\",\"cityCode\":\"\(selectedCityCode)\",\"name\":\"\(tf_first_name.text!)\",\"lastName\":\"\(tf_last_name.text!)\",\"companyName\":\"\(tf_company_name.text!)\",\"Image\":\"\(image)\",\"typeOfProduct\":\"\(tf_product_type.text!)\",\"country\":\"\(tf_country.text!)\",\"city\":\"\(tf_city.text!)\",\"about\":\"\(str)\",\"branch\":\"\(tf_branch.text!)\"},\"function_name\":\"completeProfile\"}"
        
//        print(param)
        
        APIManager.apiRequest(Parameters: param) { jsonResponse, error in
            
//            print(jsonResponse!)
            if jsonResponse?.count != 0 {
                let msg : String = (jsonResponse!.value(forKey: "message") as? String)!
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
                if jsonResponse?.value(forKey: "msg_code") as? Bool == true {
                    
                    let image = ""
                    
                    let loginUserInfo = ["type": SharedData.instance.selected_user_type, "email" : email, "username" : username, "password": password, "user_id": uid, "fullName": self.tf_company_name.text!, "image": image] as [String : Any]
//                    print(loginUserInfo)
                    SharedData.instance.saveLoginUserInfoInLacal(loginUserInfo)
                    self.containerView.isHidden = false
                    
                } else {
                    if(self.currentReachabilityStatus != .notReachable) {
                        self.showAlert(title: kError_Title, message: msg)
                    } else {
                        self.showAlertInternet(title: kError_Title, message: msg, handler: {(alert: UIAlertAction!) in
                            self.performCompleteProfile()
                        })
                    }
                }
            }
            return
        }

    }
    
    @IBAction func popUpOkTapped(_ sender: Any){
        let controller: HomeCompanyVC = self.storyboard?.instantiateViewController(withIdentifier: "HomeCompanyVC") as! HomeCompanyVC
        self.navigationController?.show(controller, sender: self)
       
        self.containerView.isHidden = true
    }
    
    @IBAction func didTapBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
  }
