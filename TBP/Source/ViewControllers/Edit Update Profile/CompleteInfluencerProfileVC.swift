//
//  CompleteInfluencerProfileVC.swift
//  TBP
//
//  Created by mac on 10/10/17.
//  Copyright © 2017 Impetrosys. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import FBSDKLoginKit
import Alamofire

class CompleteInfluencerProfileVC: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate, UIWebViewDelegate {

    @IBOutlet weak var tf_first_name: UITextField!
    @IBOutlet weak var tf_last_name: UITextField!
    @IBOutlet weak var tf_country: UITextField!
  
    @IBOutlet weak var tf_city: UITextField!
    @IBOutlet weak var tf_aboutMe: UITextView!
    
    @IBOutlet weak var tf_instagram: UITextField!
    @IBOutlet weak var tf_facebook: UITextField!
    @IBOutlet weak var tf_snapchat: UITextField!
    
    @IBOutlet weak var btn_male: UIButton!
    @IBOutlet weak var btn_female: UIButton!
    @IBOutlet weak var btn_other: UIButton!
    
    @IBOutlet weak var lblVerifyInst: UILabel!
    @IBOutlet weak var lblVerifyFB: UILabel!
    @IBOutlet weak var lblVerifySnapChat: UILabel!
    
    @IBOutlet weak var btnSaveUpdate: UIButton!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var loginWebView: UIWebView!
    @IBOutlet weak var instaContainer: UIView!
    
    var gender = ""
    var bool_mandatory_ac_verified = false
    
    var user_profile_photo = ""
    
    var is_facebook_verified = false
    var facebook_profile_url = ""
 
    var instagram_profile_url = ""
    var is_instagram_verified = false
    var insta_followers = 0
    
    @IBOutlet weak var containerViewCountry: UIView!
    @IBOutlet weak var countryTable: UITableView!
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var lblSelectorName: UILabel!
    
    var searchedCountryArray = [String]()
    var countriesArray = [String]()
    var countryCodeArr: NSMutableArray = NSMutableArray()
    
    var searchedCityArray = [String]()
    var citiesArray = [String]()
    var cityCodeArr: NSMutableArray = NSMutableArray()
    
    var selectedCountryCode = ""
    var changedCountryCode = ""
    var selectedCityCode = ""
    var activePicker = "country"
    
    var is_updating_profile = false
    var userData : NSDictionary = NSDictionary()
    
    @IBOutlet weak var topConstraint2: NSLayoutConstraint!
    @IBOutlet weak var topConstraint: NSLayoutConstraint!
    //MARK: -
    
    override func viewDidLoad() {
        super.viewDidLoad()
        topConstraint.constant = CGFloat(navBarHeight)
        topConstraint2.constant = CGFloat(navBarHeight)
        self.view.layoutIfNeeded()
        
        containerView.isHidden = true
        
        lblVerifyInst.attributedText = self.changeToAtributedString("Verify INSTAGRAM Account", length: 9)
        lblVerifyFB.attributedText = self.changeToAtributedString("Verify FACEBOOK Account", length: 8)
        lblVerifySnapChat.attributedText = self.changeToAtributedString("Verify SNAPCHAT Account", length: 8)
        
        self.tf_instagram.isEnabled = false
        self.tf_facebook.isEnabled = false
        self.tf_snapchat.isEnabled = false
        
        containerViewCountry.isHidden = true
        self.getCountryList()
        
        instaContainer.isHidden = true
        
       if is_updating_profile == true {
            self.parseUserData()
        }
    }
    
    func parseUserData() {
       
        if (self.userData.value(forKey: "firstname")) != nil {
            self.tf_first_name.text = self.userData.value(forKey: "firstname") as? String
        }
        if (self.userData.value(forKey: "lastName")) != nil  {
            self.tf_last_name.text = self.userData.value(forKey: "lastName") as? String
        }
        
        if (self.userData.value(forKey: "country")) != nil {
            self.tf_country.text = self.userData.value(forKey: "country") as? String
            if (self.userData.value(forKey: "countryCode")) != nil {
                selectedCountryCode = self.userData.value(forKey: "countryCode") as! String
                changedCountryCode = selectedCountryCode
            }
        }
        
        if (self.userData.value(forKey: "city")) != nil {
            self.tf_city.text = self.userData.value(forKey: "city") as? String
            if (self.userData.value(forKey: "cityCode")) != nil {
                selectedCityCode = self.userData.value(forKey: "cityCode") as! String
            }
        }
        
        if (self.userData.value(forKey: "gender")) != nil {
            gender = self.userData.value(forKey: "gender") as! String
            if gender == "female" {
                btn_female.setImage(#imageLiteral(resourceName: "radio-on-button"), for: .normal)
            } else if gender == "male"{
                btn_female.setImage(#imageLiteral(resourceName: "radio-on-button"), for: .normal)
            } else {
                btn_female.setImage(#imageLiteral(resourceName: "radio-on-button"), for: .normal)
            }
        }
        if (self.userData.value(forKey: "about")) != nil {
            self.tf_aboutMe.text = self.userData.value(forKey: "about") as? String
        }
        
        if (self.userData.value(forKey: "instagram_followers")) != nil {
            let count = self.userData.value(forKey: "instagram_followers") as? String
            if count != "" {
                self.tf_instagram.text = count
//                self.tf_instagram.isEnabled = true
            }
        }
        
        if (self.userData.value(forKey: "facebook_followers")) != nil {
            let count = self.userData.value(forKey: "facebook_followers") as? String
            if count != "" {
                self.tf_facebook.text = count
//                self.tf_facebook.isEnabled = true
            }
        }
        
        if (self.userData.value(forKey: "snapchat_followers")) != nil {
            let count = self.userData.value(forKey: "snapchat_followers") as? String
            if count != "" {
                self.tf_snapchat.text = count
//                self.tf_snapchat.isEnabled = true
            }
        }
        
        lblTitle.text = "Edit Profile"
        btnSaveUpdate.setTitle("UPDATE", for: .normal)
    }
    
    func changeToAtributedString(_ text: String, length: Int) -> NSAttributedString {
        let SCMutableString = NSMutableAttributedString(string: text)
        SCMutableString.setAttributes([NSFontAttributeName : UIFont(name: "Futura", size: CGFloat(12.0))!
            , NSForegroundColorAttributeName : kInfluencerColor], range: NSRange(location:7,length:length))
        return SCMutableString
    }

    
    //MARK: -
    
    @IBAction func selectGender(_ sender: UIButton) {
        if sender.tag == 1 {
            gender = "male"
            btn_male.setImage(#imageLiteral(resourceName: "radio-on-button"), for: .normal)
            btn_female.setImage(#imageLiteral(resourceName: "radio-off-button"), for: .normal)
            btn_other.setImage(#imageLiteral(resourceName: "radio-off-button"), for: .normal)
        }
        else if sender.tag == 2 {
            gender = "female"
            btn_female.setImage(#imageLiteral(resourceName: "radio-on-button"), for: .normal)
            btn_male.setImage(#imageLiteral(resourceName: "radio-off-button"), for: .normal)
            btn_other.setImage(#imageLiteral(resourceName: "radio-off-button"), for: .normal)
        }
        else{
            gender = "other"
            btn_other.setImage(#imageLiteral(resourceName: "radio-on-button"), for: .normal)
            btn_male.setImage(#imageLiteral(resourceName: "radio-off-button"), for: .normal)
            btn_female.setImage(#imageLiteral(resourceName: "radio-off-button"), for: .normal)
        }
    }
    
    
    //MARK: - Get Country and City list
    
    func getCountryList()
    {
        let param = "{\"function_name\":\"getCountries\"}"
        
        APIManager.apiRequest(Parameters: param) { jsonResponse, error in
            if jsonResponse?.count != 0 {
                
                let msg : String = (jsonResponse!.value(forKey: "message") as? String)!
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
                if jsonResponse?.value(forKey: "msg_code") as? Bool == true {
                    if jsonResponse?.value(forKey: "data") != nil {
                        let array = jsonResponse?.value(forKey: "data") as! NSArray
                        
                        let tempArr: NSMutableArray = NSMutableArray()
                        for item in array {
                            tempArr.add((item as AnyObject).value(forKey: "countryName")!)
                            self.countryCodeArr.add((item as AnyObject).value(forKey: "countryId")!)
                        }
                        self.countriesArray = tempArr.mutableCopy() as! [String]
                        self.searchedCountryArray = self.countriesArray
                        
                        self.countryTable.reloadData()
                        self.countryTable.setContentOffset(CGPoint.zero, animated: true)
                    }
                } else {
                    if(self.currentReachabilityStatus != .notReachable) {
                        self.showAlert(title: kError_Title, message: msg)
                    } else {
                        self.showAlertInternet(title: kError_Title, message: msg, handler: {(alert: UIAlertAction!) in
                            self.getCountryList()
                        })
                    }
                }
            }
            return
        }
    }
    
    func getCityList(countryId: String) {
        
        let param = "{\"data\":{\"countryId\":\"\(countryId)\"},\"function_name\":\"getCities\"}"
        
        APIManager.apiRequest(Parameters: param) { jsonResponse, error in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            if jsonResponse?.count != 0 {
                let msg : String = (jsonResponse!.value(forKey: "message") as? String)!
                if jsonResponse!.value(forKey: "msg_code") as! Bool == true{
                    if jsonResponse?.value(forKey: "data") != nil {

                        let array = jsonResponse?.value(forKey: "data") as! NSArray
                        
                        let tempArr: NSMutableArray = NSMutableArray()
                        for item in array {
                            let strCityNm = (item as AnyObject).value(forKey: "cityName") as! String
                            tempArr.add(strCityNm)
                            self.cityCodeArr.add((item as AnyObject).value(forKey: "cityId")!)
                        }
                        self.citiesArray = tempArr.mutableCopy() as! [String]
                        self.searchedCityArray = self.citiesArray
                        
                        self.countryTable.reloadData()
                        self.countryTable.setContentOffset(CGPoint.zero, animated: true)
                        self.containerViewCountry.isHidden = false
                        self.lblSelectorName.text = "Select Your City"
                    }
                } else {
                    if(self.currentReachabilityStatus != .notReachable) {
                        self.showAlert(title: kError_Title, message: msg)
                    } else {
                        self.showAlertInternet(title: kError_Title, message: msg, handler: {(alert: UIAlertAction!) in
                            self.getCityList(countryId: countryId)
                        })
                    }
                }
            }
            return
        }
    }
    
    //MARK: - Table View Delegates
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if activePicker == "country" {
            return self.searchedCountryArray.count
        } else {
            return self.searchedCityArray.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        
        if activePicker == "country" {
             cell.textLabel?.text = self.searchedCountryArray[indexPath.row]
        } else {
             cell.textLabel?.text = self.searchedCityArray[indexPath.row]
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if activePicker == "country" {
            let strName = searchedCountryArray[indexPath.row] as String
            let index: Int = countriesArray.index(of: strName)!
            
            if selectedCountryCode == "" {
                selectedCountryCode = (countryCodeArr[index] as? String)!
                changedCountryCode = selectedCountryCode
            } else {
                selectedCountryCode = changedCountryCode
                changedCountryCode = (countryCodeArr[index] as? String)!
            }
            tf_country.text = strName
            tf_city.text = ""
            tf_country.resignFirstResponder()
        } else {
            let strName = searchedCityArray[indexPath.row] as String
            let index: Int = citiesArray.index(of: strName)!
            selectedCityCode = (cityCodeArr[index] as? String)!
            tf_city.text = strName
            tf_aboutMe.becomeFirstResponder()
        }
        
        containerViewCountry.isHidden = true
        searchTextField.text = ""
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    // MARK: - TextField Delegates
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField == tf_country {
            activePicker = "country"
            if self.countriesArray.count != 0 {
                containerViewCountry.isHidden = false
                lblSelectorName.text = "Select Your Country"
                self.searchedCountryArray = self.countriesArray
                self.countryTable.reloadData()
                self.countryTable.setContentOffset(CGPoint.zero, animated: true)
            }
        }
        else if textField == tf_city {
            activePicker = "city"
            if selectedCountryCode != "" {
                if selectedCountryCode != changedCountryCode {
                    self.getCityList(countryId: changedCountryCode)
                } else {
                    if self.citiesArray.count != 0 {
                        self.searchedCityArray = self.citiesArray
                        self.countryTable.reloadData()
                        self.countryTable.setContentOffset(CGPoint.zero, animated: true)
                        self.containerViewCountry.isHidden = false
                        lblSelectorName.text = "Select Your City"
                    } else {
                        self.getCityList(countryId: changedCountryCode)
                    }
                }
            }
        }
    }
    
    @IBAction func searchRecordsAsPerText(_ sender: Any) {
        
        if activePicker == "country" {
            searchedCountryArray.removeAll()
            if searchTextField.text?.count != 0 {
                for strCountry in countriesArray {
                    let range = strCountry.lowercased().range(of: searchTextField.text!, options: .caseInsensitive, range: nil,   locale: nil)
                    
                    if range != nil {
                        searchedCountryArray.append(strCountry)
                    }
                }
            } else {
                searchedCountryArray = countriesArray
            }
        } else {
            searchedCityArray.removeAll()
            if searchTextField.text?.count != 0 {
                for strCountry in citiesArray {
                    let range = strCountry.lowercased().range(of: searchTextField.text!, options: .caseInsensitive, range: nil,   locale: nil)
                    
                    if range != nil {
                        searchedCityArray.append(strCountry)
                    }
                }
            } else {
                searchedCityArray = citiesArray
            }

        }
        
       countryTable.reloadData()
    }
    
    //MARK:- Verify Instagram
    
    @IBAction func loginThroughInstagram(_ sender: Any) {
//        let controller: InstagramLoginVC = self.storyboard?.instantiateViewController(withIdentifier: "InstagramLoginVC") as! InstagramLoginVC
//        self.navigationController?.show(controller, sender: self)
        instaContainer.isHidden = false
        loginWebView.delegate = self
        unSignedRequest()
    }
    
    func unSignedRequest () {
        let authURL = String(format: "%@?client_id=%@&redirect_uri=%@&response_type=token&scope=%@&DEBUG=True", arguments: [INSTAGRAM_IDS.INSTAGRAM_AUTHURL,INSTAGRAM_IDS.INSTAGRAM_CLIENT_ID,INSTAGRAM_IDS.INSTAGRAM_REDIRECT_URI, INSTAGRAM_IDS.INSTAGRAM_SCOPE ])
        let urlRequest =  URLRequest.init(url: URL.init(string: authURL)!)
        loginWebView.loadRequest(urlRequest)
    }
    
    func checkRequestForCallbackURL(request: URLRequest) -> Bool {
        
        let requestURLString = (request.url?.absoluteString)! as String
        
        if requestURLString.hasPrefix(INSTAGRAM_IDS.INSTAGRAM_REDIRECT_URI) {
            
            if requestURLString.range(of: "#access_token=") != nil {
                
                let range: Range<String.Index> = requestURLString.range(of: "#access_token=")!
                handleAuth(authToken: requestURLString.substring(from: range.upperBound))
            } else {
                self.didTapBack(self)
            }
            
            return false;
        }
        return true
    }
    
    func handleAuth(authToken: String)  {
        
        Alamofire.request("https://api.instagram.com/v1/users/self/?access_token=\(authToken)", method: .get).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case .success(_):
                self.is_instagram_verified = true
                if let JSON = response.result.value{
                    
//                    print(JSON)
                    
                    if self.user_profile_photo == ""{
                        let temp: NSDictionary = (JSON as AnyObject).value(forKey: "data") as! NSDictionary
                       
                        self.user_profile_photo = temp.value(forKey: "profile_picture") as! String
                        self.insta_followers = ((temp.value(forKey: "counts") as AnyObject).value(forKey: "followed_by") as! Int)
                        let strUnm = temp.value(forKey: "username") as! String
                        self.instagram_profile_url = "https://www.instagram.com/" + strUnm
                        
                        self.bool_mandatory_ac_verified = true
                        self.tf_instagram.text = "\(self.insta_followers)"
                        self.tf_instagram.isEnabled = true
                        self.tf_snapchat.isEnabled = true
                        
                        self.didTapCancel(self)
                    }
                }
                break
                
            case .failure(_):
                print(response.result.error?.localizedDescription ?? "fail")
                
                self.showAlert(title: kError_Title, message: response.result.error?.localizedDescription ?? "fail")
                self.is_instagram_verified = false
                self.didTapBack(self)
                break
            }
        }
    }
    
    // MARK: - UIWebViewDelegate
    
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        return checkRequestForCallbackURL(request: request)
    }
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        webViewDidFinishLoad(webView)
    }
    
    @IBAction func didTapCancel(_ sender: Any) {
        instaContainer.isHidden = true
    }
   
    //MARK:- Verify Facebook
    
    @IBAction func btnFBLoginPressed(_ sender: AnyObject) {
        let fbLoginManager : FBSDKLoginManager = FBSDKLoginManager()
        fbLoginManager.logIn(withReadPermissions: ["public_profile", "email", "user_birthday"], from: self) { (result, error) in
            if (error == nil){
                let fbloginresult : FBSDKLoginManagerLoginResult = result!
                if fbloginresult.grantedPermissions != nil {
                    if(fbloginresult.grantedPermissions.contains("email"))
                    {
                        self.getFBUserData()
                        fbLoginManager.logOut()
                    }
                }
            } else if (result?.isCancelled)! {
                print("The user cancelled loggin in")
            } else {
                print("Error loggin in is \(String(describing: error?.localizedDescription))")
            }
        }
    }
    
    func getFBUserData(){
        if((FBSDKAccessToken.current()) != nil){
            
            FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email, cover, link, gender"]).start(completionHandler: { (connection, result, error) -> Void in
                if (error == nil){
                    let arrResult : NSDictionary = result as! NSDictionary
                    
                    print(arrResult)
                    
                    self.user_profile_photo = ((arrResult.value(forKey: "picture") as AnyObject).value(forKey: "data") as AnyObject).value(forKey: "url") as! String
                    self.facebook_profile_url = (arrResult.value(forKey: "link") as? String)!
                    
                    self.bool_mandatory_ac_verified = true
                    self.is_facebook_verified = true
                    self.tf_facebook.isEnabled = true
                    self.tf_snapchat.isEnabled = true
                }
            })
        }
    }
    
  //MARK:- Verify SnapChat
  
    @IBAction func loginThroughSnapChat(_ sender: Any) {
    }
    
  //MARK: - Save Data
    
    @IBAction func saveCompanyDetails(_ sender: Any) {
        if (tf_first_name.text?.trim().isEmpty)!
        {
            self.showAlert(title: kError_Title, message: kFirst_Name)
        }
        else if (tf_last_name.text?.trim().isEmpty)!
        {
            self.showAlert(title: kError_Title, message: kLast_Name)
        }
        else if (gender == "")
        {
            self.showAlert(title: kError_Title, message: kGender)
        }
        else if (tf_country.text?.trim().isEmpty)!
        {
            self.showAlert(title: kError_Title, message: kCountry_URL)
        }
        else if (tf_city.text?.trim().isEmpty)!
        {
            self.showAlert(title: kError_Title, message: kCity_URL)
        }
        else if (tf_aboutMe.text?.trim().isEmpty)!
        {
            self.showAlert(title: kError_Title, message: kAbout)
        }
        else {
            if is_updating_profile == true {
                self.updateProfile()
            } else {
                self.performCompleteProfile()
            }
        }
    }
    
    func updateProfile() {
        if (tf_instagram.text == "") && (tf_facebook.text == "") && (tf_snapchat.text == ""){
           self.showAlert(title: kALERT, message: "Please fill atleast 1 field of social media followers.")
        } else {
            bool_mandatory_ac_verified = true
        }

        if bool_mandatory_ac_verified == true{
            
            let msgParam = ["message":"\(tf_aboutMe.text!)"] as [String : Any]
            let str = String(describing: msgParam["message"]).replacingOccurrences(of: "Optional(\"", with: "").replacingOccurrences(of: "\")", with: "")
            
            let param = "{\"data\":{\"userId\":\"\(userId)\",\"name\":\"\(tf_first_name.text!)\",\"lastName\":\"\(tf_last_name.text!)\",\"gender\":\"\(gender)\",\"about\":\"\(str)\",\"country\":\"\(tf_country.text!)\",\"city\":\"\(tf_city.text!)\",\"instagram_followers\":\"\(tf_instagram.text!)\",\"facebook_followers\":\"\(tf_facebook.text!)\",\"snapchat_followers\":\"\(tf_snapchat.text!)\",\"cityCode\":\"\(selectedCityCode)\",\"countryCode\":\"\(selectedCountryCode)\"},\"function_name\":\"edit_influenceUser\"}"
            
                APIManager.apiRequest(Parameters: param) { jsonResponse, error in
                    if jsonResponse?.count != 0 {
//                        print(jsonResponse!)
                    if jsonResponse != nil {
                        let msg : String = (jsonResponse!.value(forKey: "message") as? String)!
                        NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
                        if jsonResponse?.value(forKey: "msg_code") as? Bool == true {
                            
                            self.showAlert(title: kSuccess, message: msg, handler: {(alert: UIAlertAction!) in
                                self.didTapBack(self)
                            })
                            
                        } else {
                            if(self.currentReachabilityStatus != .notReachable) {
                                self.showAlert(title: kError_Title, message: msg)
                            } else {
                                self.showAlertInternet(title: kError_Title, message: msg, handler: {(alert: UIAlertAction!) in
                                    self.updateProfile()
                                })
                            }
                        }
                    } else {
                            self.showAlert(title: kError_Title, message: "json is nil")
                        }
                    } else {
                        self.showAlert(title: kError_Title, message: "something went wrong")
                    }
                    NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
                    return
                }
        } else {
            self.showAlert(title: kALERT, message: "Please verify atleast 1 social media account \n NOTE: Currently facebook verification is working!")
        }
    }

    
    func performCompleteProfile() {
        if bool_mandatory_ac_verified == true{
            
            if is_instagram_verified == true && (tf_instagram.text?.trim().isEmpty)!
            {
                self.showAlert(title: kError_Title, message: "Please fill number follower on instagram.")
            }
            else if is_facebook_verified == true && (tf_facebook.text?.trim().isEmpty)!{
                self.showAlert(title: kError_Title, message: "Please fill number follower on facebook.")
            }
            else
            {
                let msgParam = ["message":"\(tf_aboutMe.text!)"] as [String : Any]
                let str = String(describing: msgParam["message"]).replacingOccurrences(of: "Optional(\"", with: "").replacingOccurrences(of: "\")", with: "")
                let uid = user_login_info.value(forKey: "user_id") as! String
                let param = "{\"data\":{\"type\":\"\(SharedData.instance.selected_user_type)\",\"userId\":\"\(uid)\",\"name\":\"\(tf_first_name.text!)\",\"lastName\":\"\(tf_last_name.text!)\",\"gender\":\"\(gender)\",\"about\":\"\(str)\",\"country\":\"\(tf_country.text!)\",\"city\":\"\(tf_city.text!)\",\"instagram\":\"\(tf_instagram.text!)\",\"facebook\":\"\(tf_facebook.text!)\",\"snapchat\":\"\(tf_snapchat.text!)\",\"image\":\"\(user_profile_photo)\",\"instagram_profile_url\":\"\(instagram_profile_url)\",\"facebook_profile_url\":\"\(facebook_profile_url)\",\"cityCode\":\"\(selectedCityCode)\",\"countryCode\":\"\(selectedCountryCode)\"},\"function_name\":\"influencer_update_social\"}"
                
                APIManager.apiRequest(Parameters: param) { jsonResponse, error in
                    if jsonResponse?.count != 0 {
                        
//                        print(jsonResponse!)
                        
                        let msg : String = (jsonResponse!.value(forKey: "message") as? String)!
                        NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
                        if jsonResponse?.value(forKey: "msg_code") as? Bool == true {
                            
                            let username = user_login_info.value(forKey: "username") as! String
                            let email = user_login_info.value(forKey: "email") as! String
                            let password = user_login_info.value(forKey: "password") as! String
                            
                            let fnm = self.tf_first_name.text!
                            let lnm = self.tf_last_name.text!
                            let image = self.user_profile_photo
                            
                            let loginUserInfo = ["type": SharedData.instance.selected_user_type, "email" : email, "username" : username, "password": password, "user_id": uid, "fullName": fnm + " " + lnm, "image": image] as [String : Any]
                           
                            SharedData.instance.saveLoginUserInfoInLacal(loginUserInfo)
                            self.containerView.isHidden = false
                        } else {
                            if(self.currentReachabilityStatus != .notReachable) {
                                self.showAlert(title: kError_Title, message: msg)
                            } else {
                                self.showAlertInternet(title: kError_Title, message: msg, handler: {(alert: UIAlertAction!) in
                                    self.performCompleteProfile()
                                })
                            }
                        }
                    }
                    return
                }
            }
        } else {
            self.showAlert(title: kALERT, message: "Please verify atleast 1 social media account.")
        }
    }
    
    @IBAction func popUpOkTapped(_ sender: Any){
        let controller: HomeInfluencerVC = self.storyboard?.instantiateViewController(withIdentifier: "HomeInfluencerVC") as! HomeInfluencerVC
        self.navigationController?.show(controller, sender: self)
        
        self.containerView.isHidden = true
    }

    @IBAction func didTapBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}
