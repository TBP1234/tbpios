//
//  APIHelper.swift
//  nKryptext
//
//  Created by mac on 06/10/17.
//  Copyright © 2017 Impetrosys. All rights reserved.
//

import UIKit
import Alamofire
import NVActivityIndicatorView

class APIManager: NSObject {

    static var aPIHelper: APIManager!
    
    class func sharedInstance() -> APIManager {
        if(aPIHelper == nil) {
            aPIHelper = APIManager()
        }
        return aPIHelper
    }

    class func apiRequest(Parameters: String, completionHandler: @escaping (NSDictionary?, Error?) -> ()) {
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)
        makeCall(Parameters, completionHandler: completionHandler)
    }
    
    class func SendMessageRequest(Parameters: String, completionHandler: @escaping (NSDictionary?, Error?) -> ()) {
        makeCall(Parameters, completionHandler: completionHandler)
    }
    
    class func makeCall(_ Parameters: String, completionHandler: @escaping (NSDictionary?, Error?) -> ()){
        if(appDelegate.window?.rootViewController?.currentReachabilityStatus != .notReachable) {
            var request = URLRequest(url: URL(string: BASE_URL)!)
            request.httpMethod = HTTPMethod.post.rawValue
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            request.httpBody = (Parameters.data(using: .utf8))! as Data
            
            Alamofire.request(request).responseString { response in
                    switch response.result {
                    case .success(let value):
                        let dict = value.convertStringToDictionary()
                        completionHandler(dict, nil)
                    case .failure(let error):
                        print(error.localizedDescription)
                        let dict = ["message": error.localizedDescription, "msg_code": false] as [String : Any]
                        completionHandler(dict as NSDictionary, nil)
                    }
            }
       } else {
            let dict = ["message": "No internet connection.", "msg_code": false] as [String : Any]
            completionHandler(dict as NSDictionary, nil)
       }
    }
}
